;;; SRFI-1 list-processing library 		     -*- lexical-binding: t -*-
;;; Reference implementation
;;;
;;; Copyright (c) 1998, 1999 by Olin Shivers. You may do as you please with
;;; this code as long as you do not remove this copyright notice or
;;; hold me liable for its use. Please send bug reports to shivers@ai.mit.edu.
;;;     -Olin
;;; Copyright © 2017 Alex Vong
;;; Modified to run on Emcas. These changes are covered by the Expat license.

;;; This is a library of list- and pair-processing functions. I wrote it after
;;; carefully considering the functions provided by the libraries found in
;;; R4RS/R5RS Scheme, MIT Scheme, Gambit, RScheme, MzScheme, slib, Common
;;; Lisp, Bigloo, guile, T, APL and the SML standard basis. It is a pretty
;;; rich toolkit, providing a superset of the functionality found in any of
;;; the various Schemes I considered.

;;; This implementation is intended as a portable reference implementation
;;; for SRFI-1. See the porting notes below for more information.

;;; Exported:
;;; xcons tree-copy make-list list-tabulate cons* list-copy
;;; proper-list? circular-list? dotted-list? not-pair? null-list? list=
;;; circular-list length+
;;; iota
;;; first second third fourth fifth sixth seventh eighth ninth tenth
;;; car+cdr
;;; take       drop
;;; take-right drop-right
;;; take!      drop-right!
;;; split-at   split-at!
;;; last last-pair
;;; zip unzip1 unzip2 unzip3 unzip4 unzip5
;;; count
;;; append! append-reverse append-reverse! concatenate concatenate!
;;; unfold       fold       pair-fold       reduce
;;; unfold-right fold-right pair-fold-right reduce-right
;;; append-map append-map! map! pair-for-each filter-map map-in-order
;;; filter  partition  remove
;;; filter! partition! remove!
;;; find find-tail any every list-index
;;; take-while drop-while take-while!
;;; span break span! break!
;;; delete delete!
;;; alist-cons alist-copy
;;; delete-duplicates delete-duplicates!
;;; alist-delete alist-delete!
;;; reverse!
;;; lset<= lset= lset-adjoin
;;; lset-union  lset-intersection  lset-difference  lset-xor  lset-diff+intersection
;;; lset-union! lset-intersection! lset-difference! lset-xor! lset-diff+intersection!
;;;
;;; In principle, the following R4RS list- and pair-processing procedures
;;; are also part of this package's exports, although they are not defined
;;; in this file:
;;;   Primitives: cons pair? null? car cdr set-car! set-cdr!
;;;   Non-primitives: list length append reverse cadr ... cddddr list-ref
;;;                   memq memv assq assv
;;;   (The non-primitives are defined in this file, but commented out.)
;;;
;;; These R4RS procedures have extended definitions in SRFI-1 and are defined
;;; in this file:
;;;   map for-each member assoc
;;;
;;; The remaining two R4RS list-processing procedures are not included:
;;;   list-tail (use drop)
;;;   list? (use proper-list?)

(require 'cl-lib)
(require 'r5rs)
(require 'srfi-8)
(require 'srfi-23)

;;; A note on recursion and iteration/reversal:
;;; Many iterative list-processing algorithms naturally compute the elements
;;; of the answer list in the wrong order (left-to-right or head-to-tail) from
;;; the order needed to cons them into the proper answer (right-to-left, or
;;; tail-then-head). One style or idiom of programming these algorithms, then,
;;; loops, consing up the elements in reverse order, then destructively
;;; reverses the list at the end of the loop. I do not do this. The natural
;;; and efficient way to code these algorithms is recursively. This trades off
;;; intermediate temporary list structure for intermediate temporary stack
;;; structure. In a stack-based system, this improves cache locality and
;;; lightens the load on the GC system. Don't stand on your head to iterate!
;;; Recurse, where natural. Multiple-value returns make this even more
;;; convenient, when the recursion/iteration has multiple state values.

;;; Porting:
;;; This is carefully tuned code; do not modify casually.
;;;   - It is careful to share storage when possible;
;;;   - Side-effecting code tries not to perform redundant writes.
;;;
;;; That said, a port of this library to a specific Scheme system might wish
;;; to tune this code to exploit particulars of the implementation.
;;; The single most important compiler-specific optimisation you could make
;;; to this library would be to add rewrite rules or transforms to:
;;; - transform applications of n-ary procedures (e.g. LIST=, CONS*, APPEND,
;;;   LSET-UNION) into multiple applications of a primitive two-argument
;;;   variant.
;;; - transform applications of the mapping functions (MAP, FOR-EACH, FOLD,
;;;   ANY, EVERY) into open-coded loops. The killer here is that these
;;;   functions are n-ary. Handling the general case is quite inefficient,
;;;   requiring many intermediate data structures to be allocated and
;;;   discarded.
;;; - transform applications of procedures that take optional arguments
;;;   into calls to variants that do not take optional arguments. This
;;;   eliminates unnecessary consing and parsing of the rest parameter.
;;;
;;; These transforms would provide BIG speedups. In particular, the n-ary
;;; mapping functions are particularly slow and cons-intensive, and are good
;;; candidates for tuning. I have coded fast paths for the single-list cases,
;;; but what you really want to do is exploit the fact that the compiler
;;; usually knows how many arguments are being passed to a particular
;;; application of these functions -- they are usually explicitly called, not
;;; passed around as higher-order values. If you can arrange to have your
;;; compiler produce custom code or custom linkages based on the number of
;;; arguments in the call, you can speed these functions up a *lot*. But this
;;; kind of compiler technology no longer exists in the Scheme world as far as
;;; I can see.
;;;
;;; Note that this code is, of course, dependent upon standard bindings for
;;; the R5RS procedures -- i.e., it assumes that the variable CAR is bound
;;; to the procedure that takes the car of a list. If your Scheme
;;; implementation allows user code to alter the bindings of these procedures
;;; in a manner that would be visible to these definitions, then there might
;;; be trouble. You could consider horrible kludgery along the lines of
;;;    (define fact
;;;      (let ((= =) (- -) (* *))
;;;        (letrec ((real-fact (lambda (n)
;;;                              (if (= n 0) 1 (* n (real-fact (- n 1)))))))
;;;          real-fact)))
;;; Or you could consider shifting to a reasonable Scheme system that, say,
;;; has a module system protecting code from this kind of lossage.
;;;
;;; This code does a fair amount of run-time argument checking. If your
;;; Scheme system has a sophisticated compiler that can eliminate redundant
;;; error checks, this is no problem. However, if not, these checks incur
;;; some performance overhead -- and, in a safe Scheme implementation, they
;;; are in some sense redundant: if we don't check to see that the PROC
;;; parameter is a procedure, we'll find out anyway three lines later when
;;; we try to call the value. It's pretty easy to rip all this argument
;;; checking code out if it's inappropriate for your implementation -- just
;;; nuke every call to CHECK-ARG.
;;;
;;; On the other hand, if you *do* have a sophisticated compiler that will
;;; actually perform soft-typing and eliminate redundant checks (Rice's systems
;;; being the only possible candidate of which I'm aware), leaving these checks
;;; in can *help*, since their presence can be elided in redundant cases,
;;; and in cases where they are needed, performing the checks early, at
;;; procedure entry, can "lift" a check out of a loop.
;;;
;;; Finally, I have only checked the properties that can portably be checked
;;; with R5RS Scheme -- and this is not complete. You may wish to alter
;;; the CHECK-ARG parameter checks to perform extra, implementation-specific
;;; checks, such as procedure arity for higher-order values.
;;;
;;; The code has only these non-R4RS dependencies:
;;;   A few calls to an ERROR procedure;
;;;   Uses of the R5RS multiple-value procedure VALUES and the m-v binding
;;;     RECEIVE macro (which isn't R5RS, but is a trivial macro).
;;;   Many calls to a parameter-checking procedure check-arg:
(r5-define (s1-check-arg pred val caller)
  (r5-let lp ((val val))
    (if (funcall pred val)
        val
      (r5-tail-call lp (s23-error "Bad argument" val pred caller)))))
;;;   A few uses of the LET-OPTIONAL and :OPTIONAL macros for parsing
;;;     optional arguments.
;;;
;;; Most of these procedures use the NULL-LIST? test to trigger the
;;; base case in the inner loop or recursion. The NULL-LIST? function
;;; is defined to be a careful one -- it raises an error if passed a
;;; non-nil, non-pair value. The spec allows an implementation to use
;;; a less-careful implementation that simply defines NULL-LIST? to
;;; be NOT-PAIR?. This would speed up the inner loops of these procedures
;;; at the expense of having them silently accept dotted lists.

;;; A note on dotted lists:
;;; I, personally, take the view that the only consistent view of lists
;;; in Scheme is the view that *everything* is a list -- values such as
;;; 3 or "foo" or 'bar are simply empty dotted lists. This is due to the
;;; fact that Scheme actually has no true list type. It has a pair type,
;;; and there is an *interpretation* of the trees built using this type
;;; as lists.
;;;
;;; I lobbied to have these list-processing procedures hew to this
;;; view, and accept any value as a list argument. I was overwhelmingly
;;; overruled during the SRFI discussion phase. So I am inserting this
;;; text in the reference lib and the SRFI spec as a sort of "minority
;;; opinion" dissent.
;;;
;;; Many of the procedures in this library can be trivially redefined
;;; to handle dotted lists, just by changing the NULL-LIST? base-case
;;; check to NOT-PAIR?, meaning that any non-pair value is taken to be
;;; an empty list. For most of these procedures, that's all that is
;;; required.
;;;
;;; However, we have to do a little more work for some procedures that
;;; *produce* lists from other lists.  Were we to extend these procedures to
;;; accept dotted lists, we would have to define how they terminate the lists
;;; produced as results when passed a dotted list. I designed a coherent set
;;; of termination rules for these cases; this was posted to the SRFI-1
;;; discussion list. I additionally wrote an earlier version of this library
;;; that implemented that spec. It has been discarded during later phases of
;;; the definition and implementation of this library.
;;;
;;; The argument *against* defining these procedures to work on dotted
;;; lists is that dotted lists are the rare, odd case, and that by
;;; arranging for the procedures to handle them, we lose error checking
;;; in the cases where a dotted list is passed by accident -- e.g., when
;;; the programmer swaps a two arguments to a list-processing function,
;;; one being a scalar and one being a list. For example,
;;;     (member '(1 3 5 7 9) 7)
;;; This would quietly return #f if we extended MEMBER to accept dotted
;;; lists.
;;;
;;; The SRFI discussion record contains more discussion on this topic.


;;; Constructors
;;;;;;;;;;;;;;;;

;;; Occasionally useful as a value to be passed to a fold or other
;;; higher-order procedure.
(r5-define (s1-xcons d a)
  "Of utility only as a value to be conveniently passed to higher-order
procedures.

  (s1-xcons '(b c) 'a) => (a b c)

The name stands for \"eXchanged CONS.\""
  (cons a d))

;;;; Recursively copy every cons.
;(define (tree-copy x)
;  (let recur ((x x))
;    (if (not (pair? x)) x
;	(cons (recur (car x)) (recur (cdr x))))))

;;; Make a list of length LEN.

(r5-define (s1-make-list len &optional elt)
  "Returns an n-element list, whose elements are all the value ELT. If the ELT
argument is not given, the elements of the list may be arbitrary values.

  (s1-make-list 4 'c) => (c c c c)"
  (s1-check-arg (lambda (n) (and (r5-integer? n) (r5>= n 0)))
                len
                #'s1-make-list)
  (r5-do ((i len (r5- i 1))
          (ans '() (cons elt ans)))
         ((r5<= i 0) ans)))


;(define (list . ans) ans)	; R4RS


;;; Make a list of length LEN. Elt i is (PROC i) for 0 <= i < LEN.

(r5-define (s1-list-tabulate len proc)
  "Returns an LEN-element list. Element I of the list, where 0 <= I < LEN, is
produced by (funcall PROC I). No guarantee is made about the dynamic order in
which PROC is applied to these indices.

  (s1-list-tabulate 4 #'r5-values) => (0 1 2 3)"
  (s1-check-arg (lambda (n) (and (r5-integer? n) (r5>= n 0)))
                len
                #'s1-list-tabulate)
  (s1-check-arg #'r5-procedure? proc #'s1-list-tabulate)
  (r5-do ((i (r5- len 1) (r5- i 1))
          (ans '() (cons (funcall proc i) ans)))
         ((r5< i 0) ans)))

;;; (cons* a1 a2 ... an) = (cons a1 (cons a2 (cons ... an)))
;;; (cons* a1) = a1	(cons* a1 a2 ...) = (cons a1 (cons* a2 ...))
;;;
;;; (cons first (unfold not-pair? car cdr rest values))

(r5-define (s1-cons* first &rest rest)
  "Like `list', but the last argument provides the tail of the constructed
list, returning

  (cons ELT1 (cons ELT2 (cons ... ELTN)))

This function is called `list*' in Common Lisp and about half of the Schemes
that provide it, and `cons*' in the other half.

  (s1-cons* 1 2 3 4) => (1 2 3 . 4)
  (s1-cons* 1) => 1"
  (r5-let recur ((x first) (rest rest))
    (if (r5-pair? rest)
	(cons x (funcall recur (car rest) (cdr rest)))
      x)))

;;; (unfold not-pair? car cdr lis values)

(r5-define (s1-list-copy lis)
  "Copies the spine of the argument."
  (r5-let recur ((lis lis))
    (if (r5-pair? lis)
	(cons (car lis) (funcall recur (cdr lis)))
      lis)))

;;; IOTA count [start step]	(start start+step ... start+(count-1)*step)

(r5-define (s1-iota count &optional start step)
  "Returns a list containing the elements

  (START START+STEP ... START+(COUNT-1)*STEP)

The START and STEP parameters default to 0 and 1, respectively. This procedure
takes its name from the APL primitive.

  (s1-iota 5) => (0 1 2 3 4)

  (s1-iota 5 0 -0.1)
  => ((float 0 0) (float -1 -1) (float -2 -1) (float -3 -1) (float -4 -1))"
  (s1-check-arg #'r5-integer? count #'s1-iota)
  (if (r5< count 0) (s23-error "Negative step count" #'s1-iota count))
  (r5-let ((start (if (r5-null? start) 0 start))
           (step (if (r5-null? step) 1 step)))
    (s1-check-arg #'r5-number? start #'s1-iota)
    (s1-check-arg #'r5-number? step #'s1-iota)
    (r5-let loop ((n 0) (r '()))
      (if (r5= n count)
	  (reverse r)
        (r5-tail-call loop
                      (r5+ 1 n)
                      (cons (r5+ start (r5* n step)) r))))))

;;; I thought these were lovely, but the public at large did not share my
;;; enthusiasm...
;;; :IOTA to		(0 ... to-1)
;;; :IOTA from to	(from ... to-1)
;;; :IOTA from to step  (from from+step ...)

;;; IOTA: to		(1 ... to)
;;; IOTA: from to	(from+1 ... to)
;;; IOTA: from to step	(from+step from+2step ...)

;(define (%parse-iota-args arg1 rest-args proc)
;  (let ((check (lambda (n) (check-arg integer? n proc))))
;    (check arg1)
;    (if (pair? rest-args)
;	(let ((arg2 (check (car rest-args)))
;	      (rest (cdr rest-args)))
;	  (if (pair? rest)
;	      (let ((arg3 (check (car rest)))
;		    (rest (cdr rest)))
;		(if (pair? rest) (error "Too many parameters" proc arg1 rest-args)
;		    (values arg1 arg2 arg3)))
;	      (values arg1 arg2 1)))
;	(values 0 arg1 1))))
;
;(define (iota: arg1 . rest-args)
;  (receive (from to step) (%parse-iota-args arg1 rest-args iota:)
;    (let* ((numsteps (floor (/ (- to from) step)))
;	   (last-val (+ from (* step numsteps))))
;      (if (< numsteps 0) (error "Negative step count" iota: from to step))
;      (do ((steps-left numsteps (- steps-left 1))
;	   (val last-val (- val step))
;	   (ans '() (cons val ans)))
;	  ((<= steps-left 0) ans)))))
;
;
;(define (:iota arg1 . rest-args)
;  (receive (from to step) (%parse-iota-args arg1 rest-args :iota)
;    (let* ((numsteps (ceiling (/ (- to from) step)))
;	   (last-val (+ from (* step (- numsteps 1)))))
;      (if (< numsteps 0) (error "Negative step count" :iota from to step))
;      (do ((steps-left numsteps (- steps-left 1))
;	   (val last-val (- val step))
;	   (ans '() (cons val ans)))
;	  ((<= steps-left 0) ans)))))



(r5-define (s1-circular-list val1 &rest vals)
  "Constructs a circular list of the elements.

  (s1-circular-list 'z 'q) => (z q z q z q ...)"
  (r5-let ((ans (cons val1 vals)))
    (r5-set-cdr! (s1-last-pair ans) ans)
    ans))

;;; <proper-list> ::= ()			; Empty proper list
;;;		  |   (cons <x> <proper-list>)	; Proper-list pair
;;; Note that this definition rules out circular lists -- and this
;;; function is required to detect this case and return false.

(r5-define (s1-proper-list? x)
  "Returns true iff X is a proper list -- a finite, nil-terminated list.

More carefully: The empty list is a proper list. A pair whose cdr is a proper
list is also a proper list:

  <PROPER-LIST> ::= ()                            (Empty proper list)
                |   (cons <X> <PROPER-LIST>)      (Proper-list pair)

Note that this definition rules out circular lists. This function is required
to detect this case and return false.

Nil-terminated lists are called \"proper\" lists by R5RS and Common Lisp. The
opposite of proper is improper.

R5RS binds this function to the variable `list?'.

  (not (s1-proper-list? X)) = (or (s1-dotted-list? X) (s1-circular-list? X))"
  (r5-let lp ((x x) (lag x))
    (if (r5-pair? x)
	(r5-let ((x (cdr x)))
	  (if (r5-pair? x)
	      (r5-let ((x   (cdr x))
                       (lag (cdr lag)))
		(and (not (r5-eq? x lag)) (r5-tail-call lp x lag)))
            (r5-null? x)))
      (r5-null? x))))


;;; A dotted list is a finite list (possibly of length 0) terminated
;;; by a non-nil value. Any non-cons, non-nil value (e.g., "foo" or 5)
;;; is a dotted list of length 0.
;;;
;;; <dotted-list> ::= <non-nil,non-pair>	; Empty dotted list
;;;               |   (cons <x> <dotted-list>)	; Proper-list pair

(r5-define (s1-dotted-list? x)
  "True if X is a finite, non-nil-terminated list. That is, there exists an
n >= 0 such that cdr^n(X) is neither a pair nor (). This includes non-pair,
non-() values (e.g. symbols, numbers), which are considered to be dotted lists
of length 0.

  (not (s1-dotted-list? X)) = (or (s1-proper-list? X) (s1-circular-list? X))"
  (r5-let lp ((x x) (lag x))
    (if (r5-pair? x)
	(r5-let ((x (cdr x)))
	  (if (r5-pair? x)
	      (r5-let ((x   (cdr x))
                       (lag (cdr lag)))
		(and (not (r5-eq? x lag)) (r5-tail-call lp x lag)))
            (not (r5-null? x))))
      (not (r5-null? x)))))

(r5-define (s1-circular-list? x)
  "True if X is a circular list. A circular list is a value such that for every
n >= 0, cdr^n(X) is a pair.

Terminology: The opposite of circular is finite.

  (not (s1-circular-list? X)) = (or (s1-proper-list? X) (s1-dotted-list? X))"
  (r5-let lp ((x x) (lag x))
    (and (r5-pair? x)
	 (r5-let ((x (cdr x)))
	   (and (r5-pair? x)
		(r5-let ((x   (cdr x))
                         (lag (cdr lag)))
		  (or (r5-eq? x lag) (r5-tail-call lp x lag))))))))

(r5-define (s1-not-pair? x)
  "= (not (r5-pair? X))

Provided as a procedure as it can be useful as the termination condition for
list-processing procedures that wish to handle all finite lists, both proper
and dotted."
  (not (r5-pair? x)))	; Inline me.

;;; This is a legal definition which is fast and sloppy:
;;;     (define null-list? not-pair?)
;;; but we'll provide a more careful one:
(r5-define (s1-null-list? l)
  "List is a proper or circular list. This procedure returns true if the
argument is the empty list (), and false otherwise. It is an error to pass this
procedure a value which is not a proper or circular list. This procedure is
recommended as the termination condition for list-processing procedures that
are not defined on dotted lists."
  (r5-cond ((r5-pair? l) '())
           ((r5-null? l) t)
           (else (s23-error "s1-null-list?: argument out of domain" l))))


(r5-define (s1-list= elt= &rest lists)
  "Determines list equality, given an element-equality procedure. Proper list A
equals proper list B if they are of the same length, and their corresponding
elements are equal, as determined by ELT=. If the element-comparison
procedure's first argument is from LIST_I, then its second argument is from
LIST_I+1, i.e. it is always called as (funcall ELT= ALPHA BETA) for ALPHA an
element of list A, and BETA an element of list B.

In the N-ary case, every LIST_I is compared to LIST_I+1 (as opposed, for
example, to comparing LIST_1 to every LIST_I, for I > 1). If there are no list
arguments at all, `s1-list=' simply returns true.

It is an error to apply `s1-list=' to anything except proper lists. While
implementations may choose to extend it to circular lists, note that it cannot
reasonably be extended to dotted lists, as it provides no way to specify an
equality procedure for comparing the list terminators.

Note that the dynamic order in which the ELT= procedure is applied to pairs of
elements is not specified. For example, if `s1-list=' is applied to three
lists, A, B, and C, it may first completely compare A to B, then compare B to
C, or it may compare the first elements of A and B, then the first elements of
B and C, then the second elements of A and B, and so forth.

The equality procedure must be consistent with `r5-eq?'. That is, it must be
the case that

  (r5-eq? X Y) => (funcall ELT= X Y).

Note that this implies that two lists which are `r5-eq?' are always `s1-list=',
as well; implementations may exploit this fact to \"short-cut\" the
element-by-element comparisons.

  (s1-list= #'r5-eq?) => t       ; Trivial cases
  (s1-list= #'r5-eq? '(a)) => t"
  (or (r5-null? lists) ; special case

      (r5-let lp1 ((list-a (car lists)) (others (cdr lists)))
	(or (r5-null? others)
	    (r5-let ((list-b (car others))
                     (others (cdr others)))
	      (if (r5-eq? list-a list-b)	; EQ? => LIST=
		  (r5-tail-call lp1 list-b others)
                (r5-let lp2 ((pair-a list-a) (pair-b list-b))
                  (if (s1-null-list? pair-a)
                      (and (s1-null-list? pair-b)
                           (funcall lp1 list-b others))
                    (and (not (s1-null-list? pair-b))
                         (funcall elt= (car pair-a) (car pair-b))
                         (r5-tail-call lp2 (cdr pair-a) (cdr pair-b)))))))))))


;;; R4RS, so commented out.
;(define (length x)			; LENGTH may diverge or
;  (let lp ((x x) (len 0))		; raise an error if X is
;    (if (pair? x)			; a circular list. This version
;        (lp (cdr x) (+ len 1))		; diverges.
;        len)))

(r5-define (s1-length+ x)			; Returns #f if X is circular.
  "`s1-length+' returns the length of the argument.

`s1-length+' returns () when applied to a circular list.

The length of a proper list is a non-negative integer N such that `cdr' applied
N times to the list produces the empty list."
  (r5-let lp ((x x) (lag x) (len 0))
    (if (r5-pair? x)
	(r5-let ((x (cdr x))
                 (len (r5+ len 1)))
	  (if (r5-pair? x)
	      (r5-let ((x   (cdr x))
                       (lag (cdr lag))
                       (len (r5+ len 1)))
		(and (not (r5-eq? x lag)) (r5-tail-call lp x lag len)))
            len))
      len)))

(r5-define (s1-zip list1 &rest more-lists)
  "= (s1-map #'list (cons LIST1 MORE-LISTS))

If `s1-zip' is passed N lists, it returns a list as long as the shortest of
these lists, each element of which is an N-element list comprised of the
corresponding elements from the parameter lists.

  (s1-zip '(one two three)
          '(1 2 3)
          '(odd even odd even odd even odd even))
  => ((one 1 odd) (two 2 even) (three 3 odd))

  (s1-zip '(1 2 3)) => ((1) (2) (3))

At least one of the argument lists must be finite:

  (s1-zip '(3 1 4 1) (s1-circular-list '() t)) => ((3 ()) (1 ()) (4 ()) (1 t))"
  (apply #'s1-map #'list list1 more-lists))


;;; Selectors
;;;;;;;;;;;;;

;;; R4RS non-primitives:
;(define (caar   x) (car (car x)))
;(define (cadr   x) (car (cdr x)))
;(define (cdar   x) (cdr (car x)))
;(define (cddr   x) (cdr (cdr x)))
;
;(define (caaar  x) (caar (car x)))
;(define (caadr  x) (caar (cdr x)))
;(define (cadar  x) (cadr (car x)))
;(define (caddr  x) (cadr (cdr x)))
;(define (cdaar  x) (cdar (car x)))
;(define (cdadr  x) (cdar (cdr x)))
;(define (cddar  x) (cddr (car x)))
;(define (cdddr  x) (cddr (cdr x)))
;
;(define (caaaar x) (caaar (car x)))
;(define (caaadr x) (caaar (cdr x)))
;(define (caadar x) (caadr (car x)))
;(define (caaddr x) (caadr (cdr x)))
;(define (cadaar x) (cadar (car x)))
;(define (cadadr x) (cadar (cdr x)))
;(define (caddar x) (caddr (car x)))
;(define (cadddr x) (caddr (cdr x)))
;(define (cdaaar x) (cdaar (car x)))
;(define (cdaadr x) (cdaar (cdr x)))
;(define (cdadar x) (cdadr (car x)))
;(define (cdaddr x) (cdadr (cdr x)))
;(define (cddaar x) (cddar (car x)))
;(define (cddadr x) (cddar (cdr x)))
;(define (cdddar x) (cdddr (car x)))
;(define (cddddr x) (cdddr (cdr x)))


(defalias 's1-first  #'car)
(defalias 's1-second #'cadr)
(defalias 's1-third  #'r5-caddr)
(defalias 's1-fourth #'r5-cadddr)
(r5-define (s1-fifth   x)
  "Returns the 5th element of X. This is the same as the car of (s1-drop X 5)."
  (car    (r5-cddddr x)))
(r5-define (s1-sixth   x)
  "Returns the 6th element of X. This is the same as the car of (s1-drop X 6)."
  (cadr   (r5-cddddr x)))
(r5-define (s1-seventh x)
  "Returns the 7th element of X. This is the same as the car of (s1-drop X 7)."
  (r5-caddr  (r5-cddddr x)))
(r5-define (s1-eighth  x)
  "Returns the 8th element of X. This is the same as the car of (s1-drop X 8)."
  (r5-cadddr (r5-cddddr x)))
(r5-define (s1-ninth   x)
  "Returns the 9th element of X. This is the same as the car of (s1-drop X 9)."
  (car  (r5-cddddr (r5-cddddr x))))
(r5-define (s1-tenth   x)
  "Returns the 10th element of X. This is the same as the car of (s1-drop X 10)."
  (cadr (r5-cddddr (r5-cddddr x))))

(r5-define (s1-car+cdr pair)
  "The fundamental pair deconstructor:

  (r5-values (car PAIR) (cdr PAIR))

This can, of course, be implemented more efficiently by a compiler."
  (r5-values (car pair) (cdr pair)))

;;; take & drop

(r5-define (s1-take lis k)
  "`s1-take' returns the first K elements of list LIS.

  (s1-take '(a b c d e)  2) => (a b)

LIS may be any value -- a proper, circular, or dotted list:

  (s1-take '(1 2 3 . d) 2) => (1 2)
  (s1-take '(1 2 3 . d) 3) => (1 2 3)

For a legal K, `s1-take' and `s1-drop' partition the list in a manner which can
be inverted with `append':

  (append (s1-take LIS K) (s1-drop LIS K)) = LIS

If the argument is a list of non-zero length, `s1-take' is guaranteed to return
a freshly-allocated list, even in the case where the entire list is taken,
e.g. (s1-take LIS (length LIS))."
  (s1-check-arg #'r5-integer? k #'s1-take)
  (r5-let recur ((lis lis) (k k))
    (if (r5-zero? k) '()
      (cons (car lis)
            (funcall recur (cdr lis) (r5- k 1))))))

(r5-define (s1-drop lis k)
  "`s1-drop' returns all but the first K elements of list LIS.

  (s1-drop '(a b c d e)  2) => (c d e)

LIS may be any value -- a proper, circular, or dotted list:

  (s1-drop '(1 2 3 . d) 2) => (3 . d)
  (s1-drop '(1 2 3 . d) 3) => d

For a legal K, `s1-take' and `s1-drop' partition the list in a manner which can
be inverted with `append':

  (append (s1-take LIS K) (s1-drop LIS K)) = LIS

`s1-drop' is exactly equivalent to performing K `cdr' operations on LIS; the
returned value shares a common tail with LIS."
  (s1-check-arg #'r5-integer? k #'s1-drop)
  (r5-let iter ((lis lis) (k k))
    (if (r5-zero? k) lis (r5-tail-call iter (cdr lis) (r5- k 1)))))

(r5-define (s1-take! lis k)
  "`s1-take!' is \"linear-update\" variants of `s1-take': the procedure is
allowed, but not required, to alter the argument list to produce the result.

If LIS is circular, `s1-take!' may return a shorter-than-expected list:

  (s1-take! (s1-circular-list 1 3 5) 8) => (1 3)
  (s1-take! (s1-circular-list 1 3 5) 8) => (1 3 5 1 3 5 1 3)"
  (s1-check-arg #'r5-integer? k #'s1-take!)
  (if (r5-zero? k) '()
    (r5-begin (r5-set-cdr! (s1-drop lis (r5- k 1)) '())
              lis)))

;;; TAKE-RIGHT and DROP-RIGHT work by getting two pointers into the list,
;;; off by K, then chasing down the list until the lead pointer falls off
;;; the end.

(r5-define (s1-take-right lis k)
  "`s1-take-right' returns the last K elements of LIS.

  (s1-take-right '(a b c d e) 2) => (d e)

The returned list may share a common tail with the argument list.

LIS may be any finite list, either proper or dotted:

  (s1-take-right '(1 2 3 . d) 2) => (2 3 . d)
  (s1-take-right '(1 2 3 . d) 0) => d

For a legal K, `s1-take-right' and `s1-drop-right' partition the list in a
manner which can be inverted with `append':

  (append (s1-drop-right LIS K) (s1-take-right LIS K)) = LIS

`s1-take-right''s return value is guaranteed to share a common tail with LIS."
  (s1-check-arg #'r5-integer? k #'s1-take-right)
  (r5-let lp ((lag lis)  (lead (s1-drop lis k)))
    (if (r5-pair? lead)
	(r5-tail-call lp (cdr lag) (cdr lead))
      lag)))

(r5-define (s1-drop-right lis k)
  "`s1-drop-right' returns all but the last K elements of LIS.

  (s1-drop-right '(a b c d e) 2) => (a b c)

The returned list may share a common tail with the argument list.

LIS may be any finite list, either proper or dotted:

  (s1-drop-right '(1 2 3 . d) 2) => (1)
  (s1-drop-right '(1 2 3 . d) 0) => (1 2 3)

For a legal K, `s1-take-right' and `s1-drop-right' partition the list in a
manner which can be inverted with `append':

  (append (s1-drop-right LIS K) (s1-take-right LIS K)) = LIS

If the argument is a list of non-zero length, `s1-drop-right' is guaranteed to
return a freshly-allocated list, even in the case where nothing is dropped,
e.g. (s1-drop-right LIS 0)."
  (s1-check-arg #'r5-integer? k #'s1-drop-right)
  (r5-let recur ((lag lis) (lead (s1-drop lis k)))
    (if (r5-pair? lead)
	(cons (car lag) (funcall recur (cdr lag) (cdr lead)))
      '())))

;;; In this function, LEAD is actually K+1 ahead of LAG. This lets
;;; us stop LAG one step early, in time to smash its cdr to ().
(r5-define (s1-drop-right! lis k)
  "`s1-drop-right!' is \"linear-update\" variants of `s1-drop-right': the
procedure is allowed, but not required, to alter the argument list to produce
the result."
  (s1-check-arg #'r5-integer? k #'s1-drop-right!)
  (r5-let ((lead (s1-drop lis k)))
    (if (r5-pair? lead)

	(r5-let lp ((lag lis)  (lead (cdr lead)))	; Standard case
	  (if (r5-pair? lead)
	      (r5-tail-call lp (cdr lag) (cdr lead))
            (r5-begin (r5-set-cdr! lag '())
                      lis)))

      '())))	; Special case dropping everything -- no cons to side-effect.

;(define (list-ref lis i) (car (drop lis i)))	; R4RS

;;; These use the APL convention, whereby negative indices mean
;;; "from the right." I liked them, but they didn't win over the
;;; SRFI reviewers.
;;; K >= 0: Take and drop  K elts from the front of the list.
;;; K <= 0: Take and drop -K elts from the end   of the list.

;(define (take lis k)
;  (check-arg integer? k take)
;  (if (negative? k)
;      (list-tail lis (+ k (length lis)))
;      (let recur ((lis lis) (k k))
;	(if (zero? k) '()
;	    (cons (car lis)
;		  (recur (cdr lis) (- k 1)))))))
;
;(define (drop lis k)
;  (check-arg integer? k drop)
;  (if (negative? k)
;      (let recur ((lis lis) (nelts (+ k (length lis))))
;	(if (zero? nelts) '()
;	    (cons (car lis)
;		  (recur (cdr lis) (- nelts 1)))))
;      (list-tail lis k)))
;
;
;(define (take! lis k)
;  (check-arg integer? k take!)
;  (cond ((zero? k) '())
;	((positive? k)
;	 (set-cdr! (list-tail lis (- k 1)) '())
;	 lis)
;	(else (list-tail lis (+ k (length lis))))))
;
;(define (drop! lis k)
;  (check-arg integer? k drop!)
;  (if (negative? k)
;      (let ((nelts (+ k (length lis))))
;	(if (zero? nelts) '()
;	    (begin (set-cdr! (list-tail lis (- nelts 1)) '())
;		   lis)))
;      (list-tail lis k)))

(r5-define (s1-split-at x k)
  "`s1-split-at' splits the list X at index K, returning a list of the first K
elements, and the remaining tail. It is equivalent to

  (r5-values (s1-take X K) (s1-drop X K))

  (s1-split-at '(a b c d e f g h) 3)
  => (a b c)
     (d e f g h)"
  (s1-check-arg #'r5-integer? k #'s1-split-at)
  (r5-let recur ((lis x) (k k))
    (if (r5-zero? k) (r5-values '() lis)
      (s8-receive (prefix suffix) (funcall recur (cdr lis) (r5- k 1))
        (r5-values (cons (car lis) prefix) suffix)))))

(r5-define (s1-split-at! x k)
  "`s1-split-at!' is the linear-update variant of `s1-split-at'. It is allowed,
but not required, to alter the argument list to produce the result."
  (s1-check-arg #'r5-integer? k #'s1-split-at!)
  (if (r5-zero? k) (r5-values '() x)
    (let* ((prev (s1-drop x (r5- k 1)))
           (suffix (cdr prev)))
      (r5-set-cdr! prev '())
      (r5-values x suffix))))


(r5-define (s1-last lis)
  "`s1-last' returns the last element of the non-empty, finite list pair.

  (s1-last '(a b c)) => c"
  (car (s1-last-pair lis)))

(r5-define (s1-last-pair lis)
  "`s1-last-pair' returns the last pair in the non-empty, finite list pair.

  (s1-last-pair '(a b c)) => (c)"
  (s1-check-arg #'r5-pair? lis #'s1-last-pair)
  (r5-let lp ((lis lis))
    (r5-let ((tail (cdr lis)))
      (if (r5-pair? tail) (r5-tail-call lp tail) lis))))


;;; Unzippers -- 1 through 5
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(r5-define (s1-unzip1 lis)
  "`s1-unzip1' takes a list of lists, where every list must contain at least
one element, and returns a list containing the initial element of each such
list. That is, it returns (s1-map #'car LIS)."
  (s1-map #'car lis))

(r5-define (s1-unzip2 lis)
  "`s1-unzip2' takes a list of lists, where every list must contain at least
two elements, and returns two values: a list of the first elements, and a list
of the second elements."
  (r5-let recur ((lis lis))
    (if (s1-null-list? lis) (r5-values lis lis)	; Use NOT-PAIR? to handle
      (r5-let ((elt (car lis)))			; dotted lists.
        (s8-receive (a b) (funcall recur (cdr lis))
          (r5-values (cons (car  elt) a)
                     (cons (cadr elt) b)))))))

(r5-define (s1-unzip3 lis)
  "`s1-unzip3' takes a list of lists, where every list must contain at least
three elements, and returns three values: a list of the first elements, a list
of the second elements and a list of the third elements."
  (r5-let recur ((lis lis))
    (if (s1-null-list? lis) (r5-values lis lis lis)
      (r5-let ((elt (car lis)))
        (s8-receive (a b c) (funcall recur (cdr lis))
          (r5-values (cons (car   elt) a)
                     (cons (cadr  elt) b)
                     (cons (r5-caddr elt) c)))))))

(r5-define (s1-unzip4 lis)
  "`s1-unzip4' takes a list of lists, where every list must contain at least
four elements, and returns four values: a list of the first elements, a list of
the second elements, a list of the third elements and a list of the fourth
elements."
  (r5-let recur ((lis lis))
    (if (s1-null-list? lis) (r5-values lis lis lis lis)
      (r5-let ((elt (car lis)))
        (s8-receive (a b c d) (funcall recur (cdr lis))
          (r5-values (cons (car    elt) a)
                     (cons (cadr   elt) b)
                     (cons (r5-caddr  elt) c)
                     (cons (r5-cadddr elt) d)))))))

(r5-define (s1-unzip5 lis)
  "`s1-unzip5' takes a list of lists, where every list must contain at least
five elements, and returns five values: a list of the first elements, a list of
the second elements, a list of the third elements, a list of the fourth
elements and a list of the fifth elements."

  (r5-let recur ((lis lis))
    (if (s1-null-list? lis) (r5-values lis lis lis lis lis)
      (r5-let ((elt (car lis)))
        (s8-receive (a b c d e) (funcall recur (cdr lis))
          (r5-values (cons (car     elt) a)
                     (cons (cadr    elt) b)
                     (cons (r5-caddr   elt) c)
                     (cons (r5-cadddr  elt) d)
                     (cons (car (r5-cddddr  elt)) e)))))))


;;; append! append-reverse append-reverse! concatenate concatenate!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(r5-define (s1-append! &rest lists)
  "`s1-append!' is the \"linear-update\" variant of `append' -- it is allowed,
but not required, to alter cons cells in the argument lists to construct the
result list. The last argument is never altered; the result list shares
structure with this parameter."
  ;; First, scan through lists looking for a non-empty one.
  (r5-let lp ((lists lists) (prev '()))
    (if (not (r5-pair? lists)) prev
      (r5-let ((first (car lists))
               (rest (cdr lists)))
        (if (not (r5-pair? first)) (r5-tail-call lp rest first)

          ;; Now, do the splicing.
          (r5-let lp2 ((tail-cons (s1-last-pair first))
                       (rest rest))
            (if (r5-pair? rest)
                (r5-let ((next (car rest))
                         (rest (cdr rest)))
                  (r5-set-cdr! tail-cons next)
                  (r5-tail-call lp2
                                (if (r5-pair? next)
                                    (s1-last-pair next)
                                  tail-cons)
                                rest))
              first)))))))

;;; APPEND is R4RS.
;(define (append . lists)
;  (if (pair? lists)
;      (let recur ((list1 (car lists)) (lists (cdr lists)))
;        (if (pair? lists)
;            (let ((tail (recur (car lists) (cdr lists))))
;              (fold-right cons tail list1)) ; Append LIST1 & TAIL.
;            list1))
;      '()))

;(define (append-reverse rev-head tail) (fold cons tail rev-head))

;(define (append-reverse! rev-head tail)
;  (pair-fold (lambda (pair tail) (set-cdr! pair tail) pair)
;             tail
;             rev-head))

;;; Hand-inline the FOLD and PAIR-FOLD ops for speed.

(r5-define (s1-append-reverse rev-head tail)
  "`s1-append-reverse' returns (append (reverse REV-HEAD) TAIL). It is provided
because it is a common operation -- a common list-processing style calls for
this exact operation to transfer values accumulated in reverse order onto the
front of another list, and because the implementation is significantly more
efficient than the simple composition it replaces. (But note that this pattern
of iterative computation followed by a `reverse' can frequently be rewritten as
a recursion, dispensing with the `reverse' and `s1-append-reverse' steps, and
shifting temporary, intermediate storage from the heap to the stack, which is
typically a win for reasons of cache locality and eager storage reclamation.)"
  (r5-let lp ((rev-head rev-head) (tail tail))
    (if (s1-null-list? rev-head) tail
      (r5-tail-call lp (cdr rev-head) (cons (car rev-head) tail)))))

(r5-define (s1-append-reverse! rev-head tail)
  "`s1-append-reverse!' is just the linear-update variant of
`s1-append-reverse' -- it is allowed, but not required, to alter REV-HEAD's
cons cells to construct the result."
  (r5-let lp ((rev-head rev-head) (tail tail))
    (if (s1-null-list? rev-head) tail
      (r5-let ((next-rev (cdr rev-head)))
        (r5-set-cdr! rev-head tail)
        (r5-tail-call lp next-rev rev-head)))))


(r5-define (s1-concatenate  lists)
  "This function appends the elements of their argument together. That is,
`s1-concatenate' returns

  (apply #'append LISTS)

or, equivalently,

  (s1-reduce-right #'append '() LISTS)

Note that some Scheme implementations do not support passing more than a
certain number (e.g., 64) of arguments to an N-ary procedure. In these
implementations, the (apply append ...) idiom would fail when applied to long
lists, but `s1-concatenate' would continue to function properly.

As with `append', the last element of the input list may be any value at all."
  (s1-reduce-right #'append  '() lists))
(r5-define (s1-concatenate! lists)
  "This function appends the elements of their argument together.

`s1-concatenate!' is the linear-update variant of `s1-concatenate', defined in
terms of `s1-append!' instead of `append'.

As with `s1-append!', the last element of the input list may be any value at
all."
  (s1-reduce-right #'s1-append! '() lists))

;;; Fold/map internal utilities
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; These little internal utilities are used by the general
;;; fold & mapper funs for the n-ary cases . It'd be nice if they got inlined.
;;; One the other hand, the n-ary cases are painfully inefficient as it is.
;;; An aggressive implementation should simply re-write these functions
;;; for raw efficiency; I have written them for as much clarity, portability,
;;; and simplicity as can be achieved.
;;;
;;; I use the dreaded call/cc to do local aborts. A good compiler could
;;; handle this with extreme efficiency. An implementation that provides
;;; a one-shot, non-persistent continuation grabber could help the compiler
;;; out by using that in place of the call/cc's in these routines.
;;;
;;; These functions have funky definitions that are precisely tuned to
;;; the needs of the fold/map procs -- for example, to minimize the number
;;; of times the argument lists need to be examined.

;;; Return (map cdr lists).
;;; However, if any element of LISTS is empty, just abort and return '().
(r5-define (s1-%cdrs lists)
  (r5-let ((abort (cl-gensym)))
    (catch abort
      (r5-let recur ((lists lists))
	(if (r5-pair? lists)
	    (r5-let ((lis (car lists)))
	      (if (s1-null-list? lis) (throw abort '())
                (cons (cdr lis) (funcall recur (cdr lists)))))
          '())))))

(r5-define (s1-%cars+ lists last-elt)	; (append! (map car lists) (list last-elt))
  (r5-let recur ((lists lists))
    (if (r5-pair? lists)
        (cons (caar lists) (funcall recur (cdr lists)))
      (list last-elt))))

;;; LISTS is a (not very long) non-empty list of lists.
;;; Return two lists: the cars & the cdrs of the lists.
;;; However, if any of the lists is empty, just abort and return [() ()].

(r5-define (s1-%cars+cdrs lists)
  (r5-let ((abort (cl-gensym)))
    (catch abort
      (r5-let recur ((lists lists))
        (if (r5-pair? lists)
	    (s8-receive (list other-lists) (s1-car+cdr lists)
	      (if (s1-null-list? list) (throw abort (r5-values '() '())) ; LIST is empty -- bail out
                (s8-receive (a d) (s1-car+cdr list)
                  (s8-receive (cars cdrs) (funcall recur other-lists)
                    (r5-values (cons a cars) (cons d cdrs))))))
          (r5-values '() '()))))))

;;; Like %CARS+CDRS, but we pass in a final elt tacked onto the end of the
;;; cars list. What a hack.
(r5-define (s1-%cars+cdrs+ lists cars-final)
  (r5-let ((abort (cl-gensym)))
    (catch abort
      (r5-let recur ((lists lists))
        (if (r5-pair? lists)
	    (s8-receive (list other-lists) (s1-car+cdr lists)
	      (if (s1-null-list? list) (throw abort (r5-values '() '())) ; LIST is empty -- bail out
                (s8-receive (a d) (s1-car+cdr list)
                  (s8-receive (cars cdrs) (funcall recur other-lists)
                    (r5-values (cons a cars) (cons d cdrs))))))
          (r5-values (list cars-final) '()))))))

;;; Like %CARS+CDRS, but blow up if any list is empty.
(r5-define (s1-%cars+cdrs/no-test lists)
  (r5-let recur ((lists lists))
    (if (r5-pair? lists)
	(s8-receive (list other-lists) (s1-car+cdr lists)
	  (s8-receive (a d) (s1-car+cdr list)
	    (s8-receive (cars cdrs) (funcall recur other-lists)
	      (r5-values (cons a cars) (cons d cdrs)))))
      (r5-values '() '()))))


;;; count
;;;;;;;;;
(r5-define (s1-count pred list1 &rest lists)
  "PRED is a procedure taking as many arguments as there are lists and
returning a single value. It is applied element-wise to the elements of the
lists, and a count is tallied of the number of elements that produce a true
value. This count is returned. count is \"iterative\" in that it is guaranteed
to apply PRED to the list elements in a left-to-right order. The counting stops
when the shortest list expires.

  (s1-count #'r5-even? '(3 1 4 1 5 9 2 5 6)) => 3
  (s1-count #'r5< '(1 2 4 8) '(2 4 6 8 10 12 14 16)) => 3

At least one of the argument lists must be finite:

  (s1-count #'r5< '(3 1 4 1) (s1-circular-list 1 10)) => 2"
  (s1-check-arg #'r5-procedure? pred #'s1-count)
  (if (r5-pair? lists)

      ;; N-ary case
      (r5-let lp ((list1 list1) (lists lists) (i 0))
	(if (s1-null-list? list1) i
          (s8-receive (as ds) (s1-%cars+cdrs lists)
            (if (r5-null? as) i
              (r5-tail-call lp
                            (cdr list1)
                            ds
                            (if (apply pred (car list1) as) (r5+ i 1) i))))))

    ;; Fast path
    (r5-let lp ((lis list1) (i 0))
      (if (s1-null-list? lis) i
        (r5-tail-call lp
                      (cdr lis)
                      (if (funcall pred (car lis)) (r5+ i 1) i))))))


;;; fold/unfold
;;;;;;;;;;;;;;;

(r5-define (s1-unfold-right p f g seed &optional tail)
  "`s1-unfold-right' constructs a list with the following loop:

  (r5-let lp ((seed seed) (lis tail))
    (if (funcall P SEED) lis
        (r5-tail-call lp
                      (funcall g seed)
                      (cons (funcall f seed) lis))))

P
  Determines when to stop unfolding.

F
  Maps each seed value to the corresponding list element.

G
  Maps each seed value to next seed value.

SEED
  The \"state\" value for the unfold.

TAIL
  list terminator; defaults to '().

In other words, we use G to generate a sequence of SEED values

  SEED, G(SEED), G^2(SEED), G^3(SEED), ...

These SEED values are mapped to list elements by F, producing the elements of
the result list in a right-to-left order. P says when to stop.

`s1-unfold-right' is the fundamental iterative list constructor, just as
`s1-fold' is the fundamental iterative list consumer. While `s1-unfold-right'
may seem a bit abstract to novice functional programmers, it can be used in a
number of ways:

  ;; List of squares: 1^2 ... 10^2
  (s1-unfold-right #'r5-zero?
                   (lambda (x) (r5* x x))
                   (lambda (x) (r5- x 1))
                   10)

  ;; Reverse a proper list.
  (s1-unfold-right #'s1-null-list? #'car #'cdr LIS)

  ;; Read current input port into a list of values.
  (s1-unfold-right #'r5-eof-object? #'r5-values (lambda (x) (read)) (read))

  ;; (s1-append-reverse REV-HEAD TAIL)
  (s1-unfold-right #'s1-null-list? #'car #'cdr REV-HEAD TAIL)

Interested functional programmers may enjoy noting that `s1-fold' and
`s1-unfold-right' are in some sense inverses. That is, given operations KNULL?,
KAR, KDR, KONS, and KNIL satisfying

  (funcall KONS (funcall KAR X) (funcall KDR X)) = X
  and (funcall KNULL? KNIL) = t

then

  (s1-fold KONS KNIL (s1-unfold-right KNULL? KAR KDR X)) = X

and

  (s1-unfold-right KNULL? KAR KDR (s1-fold KONS KNIL X)) = X.

This combinator presumably has some pretentious mathematical name; interested
readers are invited to communicate it to the author."
  (s1-check-arg #'r5-procedure? p #'s1-unfold-right)
  (s1-check-arg #'r5-procedure? f #'s1-unfold-right)
  (s1-check-arg #'r5-procedure? g #'s1-unfold-right)
  (r5-let lp ((seed seed) (ans tail))
    (if (funcall p seed) ans
      (r5-tail-call lp
                    (funcall g seed)
                    (cons (funcall f seed) ans)))))


(r5-define (s1-unfold p f g seed &optional tail-gen)
  "`s1-unfold' is best described by its basic recursion:

  (s1-unfold P F G SEED)
  = (if (funcall P SEED) (funcall TAIL-GEN SEED)
        (cons (funcall F SEED)
              (s1-unfold P F G (funcall G SEED))))

P
  Determines when to stop unfolding.

F
  Maps each seed value to the corresponding list element.

G
  Maps each seed value to next seed value.

SEED
  The \"state\" value for the unfold.

TAIL-GEN
  Creates the tail of the list; defaults to (lambda (x) '())

In other words, we use G to generate a sequence of SEED values

  SEED, G(SEED), G^2(SEED), G^3(SEED), ...

These SEED values are mapped to list elements by F, producing the elements of
the result list in a left-to-right order. P says when to stop.

`s1-unfold' is the fundamental recursive list constructor, just as
`s1-fold-right' is the fundamental recursive list consumer. While `s1-unfold'
may seem a bit abstract to novice functional programmers, it can be used in a
number of ways:

  ;; List of squares: 1^2 ... 10^2
  (s1-unfold (lambda (x) (r5> x 10))
             (lambda (x) (r5* x x))
             (lambda (x) (r5+ x 1))
             1)

  (s1-unfold #'s1-null-list? #'car #'cdr LIS) ; Copy a proper list.

  ;; Read current input port into a list of values.
  (s1-unfold #'r5-eof-object? #'r5-values (lambda (x) (read)) (read))

  ;; Copy a possibly non-proper list:
  (s1-unfold #'s1-not-pair? #'car #'cdr LIS
                   #'r5-values)

  ;; Append HEAD onto TAIL:
  (s1-unfold #'s1-null-list? #'car #'cdr HEAD
                   (lambda (x) TAIL))

Interested functional programmers may enjoy noting that `s1-fold-right' and
`s1-unfold' are in some sense inverses. That is, given operations KNULL?, KAR,
KDR, KONS, and KNIL satisfying

  (funcall KONS (funcall KAR x) (funcall KDR X)) = X
  and (funcall KNULL? KNIL) = t

then

  (s1-fold-right KONS KNIL (s1-unfold KNULL? KAR KDR X)) = X

and

  (s1-unfold KNULL? KAR KDR (s1-fold-right KONS KNIL X)) = X.

This combinator sometimes is called an \"anamorphism;\" when an explicit
TAIL-GEN procedure is supplied, it is called an \"apomorphism.\""
  (s1-check-arg #'r5-procedure? p #'s1-unfold)
  (s1-check-arg #'r5-procedure? f #'s1-unfold)
  (s1-check-arg #'r5-procedure? g #'s1-unfold)
  (if (not (r5-null? tail-gen))

      (r5-let recur ((seed seed))
        (if (funcall p seed) (funcall tail-gen seed)
          (cons (funcall f seed) (funcall recur (funcall g seed)))))

    (r5-let recur ((seed seed))
      (if (funcall p seed) '()
        (cons (funcall f seed) (funcall recur (funcall g seed)))))))


(r5-define (s1-fold kons knil lis1 &rest lists)
  "The fundamental list iterator.

First, consider the single list-parameter case. If LIS1 = (E1 E2 ... EN) and
LISTS = (), then this procedure returns

  (funcall KONS EN ... (funcall KONS E2 (funcall KONS E1 KNIL)) ... )

That is, it obeys the (tail) recursion

  (s1-fold KONS KNIL LIS1)
  = (s1-fold KONS (funcall KONS (car LIS1) KNIL) (cdr LIS1))

  (s1-fold KONS KNIL '()) = KNIL

Examples:

  (s1-fold #'r5+ 0 LIS1)		; Add up the elements of LIS1.
  (s1-fold #'cons '() LIS1)		; Reverse LIS1.
  (s1-fold #'cons TAIL REV-HEAD)	; See `s1-append-reverse'.

  ;; How many symbols in LIS1?
  (s1-fold (lambda (x count) (if (r5-symbol? x) (r5+ count 1) count))
           0
           LIS1)

  ;; Length of the longest string in LIS:
  (s1-fold (lambda (s max-len) (r5-max max-len (r5-string-length s)))
           0
           LIS1)

If N list arguments are provided, then the KONS function must take N+1
parameters: one element from each list, and the \"seed\" or fold state, which
is initially KNIL. The `s1-fold' operation terminates when the shortest list
runs out of values:

  (s1-fold #'s1-cons* '() '(a b c) '(1 2 3 4 5)) => (c 3 b 2 a 1)

At least one of the list arguments must be finite."
  (s1-check-arg #'r5-procedure? kons #'s1-fold)
  (if (r5-pair? lists)
      (r5-let lp ((lists (cons lis1 lists)) (ans knil))	; N-ary case
	(s8-receive (cars+ans cdrs) (s1-%cars+cdrs+ lists ans)
	  (if (r5-null? cars+ans) ans ; Done.
            (r5-tail-call lp cdrs (apply kons cars+ans)))))

    (r5-let lp ((lis lis1) (ans knil))			; Fast path
      (if (s1-null-list? lis) ans
        (r5-tail-call lp (cdr lis) (funcall kons (car lis) ans))))))


(r5-define (s1-fold-right kons knil lis1 &rest lists)
  "The fundamental list recursion operator.

First, consider the single list-parameter case. If LIS1 = (E1 E2 ... EN) and
LISTS = (), then this procedure returns

  (funcall KONS E1 (funcall KONS E2 ... (funcall KONS EN KNIL)))

That is, it obeys the recursion

  (s1-fold-right KONS KNIL LIS1)
  = (funcall KONS (car LIS1) (s1-fold-right KONS KNIL (cdr LIS1)))

  (s1-fold-right KONS KNIL '()) = KNIL

Examples:

  (s1-fold-right #'cons '() LIS1)		; Copy LIS1.

  ;; Filter the even numbers out of LIS1.
  (s1-fold-right (lambda (x l) (if (r5-even? x) (cons x l) l)) '() LIS1)

If N list arguments are provided, then the KONS function must take N+1
parameters: one element from each list, and the \"seed\" or fold state, which
is initially KNIL. The `s1-fold' operation terminates when the shortest list
runs out of values:

  (s1-fold-right #'s1-cons* '() '(a b c) '(1 2 3 4 5)) => (a 1 b 2 c 3)

At least one of the list arguments must be finite."
  (s1-check-arg #'r5-procedure? kons #'s1-fold-right)
  (if (r5-pair? lists)
      (r5-let recur ((lists (cons lis1 lists)))		; N-ary case
	(r5-let ((cdrs (s1-%cdrs lists)))
	  (if (r5-null? cdrs) knil
            (apply kons (s1-%cars+ lists (funcall recur cdrs))))))

    (r5-let recur ((lis lis1))				; Fast path
      (if (s1-null-list? lis) knil
        (r5-let ((head (car lis)))
          (funcall kons head (funcall recur (cdr lis))))))))


(r5-define (s1-pair-fold-right f zero lis1 &rest lists)
  "Holds the same relationship with `s1-fold-right' that `s1-pair-fold' holds
with `s1-fold'. Obeys the recursion

  (s1-pair-fold-right F ZERO LIS1)
  = (funcall F LIS1 (s1-pair-fold-right KONS KNIL (cdr LIS1)))

  (s1-pair-fold-right KONS KNIL '()) = KNIL

Example:

  (s1-pair-fold-right #'cons '() '(a b c)) => ((a b c) (b c) (c))

At least one of the list arguments must be finite."
  (s1-check-arg #'r5-procedure? f #'s1-pair-fold-right)
  (if (r5-pair? lists)
      (r5-let recur ((lists (cons lis1 lists)))		; N-ary case
	(r5-let ((cdrs (s1-%cdrs lists)))
	  (if (r5-null? cdrs) zero
            (apply f (s1-append! lists (list (funcall recur cdrs)))))))

    (r5-let recur ((lis lis1))				; Fast path
      (if (s1-null-list? lis) zero (funcall f lis (funcall recur (cdr lis)))))))

(r5-define (s1-pair-fold f zero lis1 &rest lists)
  "Analogous to `s1-fold', but F is applied to successive sublists of the
lists, rather than successive elements -- that is, F is applied to the pairs
making up the lists, giving this (tail) recursion:

  (s1-pair-fold F ZERO LIS1) = (r5-let ((tail (cdr LIS1)))
                                  (s1-pair-fold F (funcall F LIS1 ZERO) tail))

  (s1-pair-fold F ZERO '()) = ZERO

For finite lists, the F function may reliably apply `r5-set-cdr!' to the pairs
it is given without altering the sequence of execution.

Example:

  ;;; Destructively reverse a list.
  (s1-pair-fold (lambda (pair tail) (r5-set-cdr! pair tail) pair) '() LIS1)

At least one of the list arguments must be finite."
  (s1-check-arg #'r5-procedure? f #'s1-pair-fold)
  (if (r5-pair? lists)
      (r5-let lp ((lists (cons lis1 lists)) (ans zero))	; N-ary case
	(r5-let ((tails (s1-%cdrs lists)))
	  (if (r5-null? tails) ans
            (r5-tail-call lp tails (apply f (s1-append! lists (list ans)))))))

    (r5-let lp ((lis lis1) (ans zero))
      (if (s1-null-list? lis) ans
        (r5-let ((tail (cdr lis)))		; Grab the cdr now,
          (r5-tail-call lp tail (funcall f lis ans)))))))	; in case F SET-CDR!s LIS.


;;; REDUCE and REDUCE-RIGHT only use RIDENTITY in the empty-list case.
;;; These cannot meaningfully be n-ary.

(r5-define (s1-reduce f ridentity lis)
  "`s1-reduce' is a variant of `s1-fold'.

RIDENTITY should be a \"right identity\" of the procedure F -- that is, for any
value X acceptable to F,

  (funcall F X RIDENTITY) = X

`s1-reduce' has the following definition:

  If LIS = (), return RIDENTITY;
  Otherwise, return (s1-fold F (car LIS) (cdr LIS)).

...in other words, we compute (s1-fold F RIDENTITY LIS).

Note that RIDENTITY is used only in the empty-list case. You typically use
`s1-reduce' when applying F is expensive and you'd like to avoid the extra
application incurred when `s1-fold' applies F to the head of list and the
identity value, redundantly producing the same value passed in to F. For
example, if F involves searching a file directory or performing a database
query, this can be significant. In general, however, `s1-fold' is useful in
many contexts where `s1-reduce' is not (consider the examples given in the
`s1-fold' definition -- only one of the five folds uses a function with a right
identity. The other four may not be performed with `s1-reduce').

Note: MIT Scheme and Haskell flip F's arg order for their `reduce' and `fold'
functions.

  ;; Take the `r5-max' of a list of non-negative integers.
  (s1-reduce #'r5-max 0 NUMS) ; i.e., (apply #'r5-max 0 NUMS)"
  (s1-check-arg #'r5-procedure? f #'s1-reduce)
  (if (s1-null-list? lis) ridentity
    (s1-fold f (car lis) (cdr lis))))

(r5-define (s1-reduce-right f ridentity lis)
  "`s1-reduce-right' is the `s1-fold-right' variant of `s1-reduce'. It obeys
the following definition:

  (s1-reduce-right F RIDENTITY '()) = RIDENTITY
  (s1-reduce-right F RIDENTITY '(E1)) = (funcall F E1 RIDENTITY) = E1

  (s1-reduce-right F RIDENTITY '(E1 E2 ...))
  = (funcall F E1 (s1-reduce F RIDENTITY (E2 ...)))

...in other words, we compute (s1-fold-right F RIDENTITY LIS).

  ;; Append a bunch of lists together.
  ;; I.e., (apply #'append LIST-OF-LISTS)
  (s1-reduce-right #'append '() LIST-OF-LISTS)"
  (s1-check-arg #'r5-procedure? f #'s1-reduce-right)
  (if (s1-null-list? lis) ridentity
    (r5-let recur ((head (car lis)) (lis (cdr lis)))
      (if (r5-pair? lis)
          (funcall f head (funcall recur (car lis) (cdr lis)))
        head))))



;;; Mappers: append-map append-map! pair-for-each map! filter-map map-in-order
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(r5-define (s1-append-map f lis1 &rest lists)
  "Equivalent to

  (apply #'append (s1-map F LIS1 LIS2 ...))

Map F over the elements of the lists, just as in the `s1-map' function.
However, the results of the applications are appended together to make the
final result. `s1-append-map' uses `append' to append the results together.

The dynamic order in which the various applications of F are made is not
specified.

At least one of the list arguments must be finite."
  (s1-really-append-map #'s1-append-map  #'append  f lis1 lists))
(r5-define (s1-append-map! f lis1 &rest lists)
  "Equivalent to

  (apply #'s1-append! (s1-map F LIS1 LIS2 ...))

Map F over the elements of the lists, just as in the `s1-map' function.
However, the results of the applications are appended together to make the
final result. `s1-append-map!' uses `s1-append!' to append the results
together.

The dynamic order in which the various applications of F are made is not
specified.

Example:

  (s1-append-map! (lambda (x) (list x (r5- x))) '(1 3 8)) => (1 -1 3 -3 8 -8)

At least one of the list arguments must be finite."
  (s1-really-append-map #'s1-append-map! #'s1-append! f lis1 lists))

(r5-define (s1-really-append-map who appender f lis1 lists)
  (s1-check-arg #'r5-procedure? f who)
  (if (r5-pair? lists)
      (s8-receive (cars cdrs) (s1-%cars+cdrs (cons lis1 lists))
	(if (r5-null? cars) '()
          (r5-let recur ((cars cars) (cdrs cdrs))
            (r5-let ((vals (apply f cars)))
              (s8-receive (cars2 cdrs2) (s1-%cars+cdrs cdrs)
                (if (r5-null? cars2) vals
                  (funcall appender vals (funcall recur cars2 cdrs2))))))))

    ;; Fast path
    (if (s1-null-list? lis1) '()
      (r5-let recur ((elt (car lis1)) (rest (cdr lis1)))
        (r5-let ((vals (funcall f elt)))
          (if (s1-null-list? rest) vals
            (funcall appender vals (funcall recur (car rest) (cdr rest)))))))))


(r5-define (s1-pair-for-each proc lis1 &rest lists)
  "Like `r5-for-each', but PROC is applied to successive sublists of the
argument lists. That is, PROC is applied to the cons cells of the lists, rather
than the lists' elements. These applications occur in left-to-right order.

The PROC procedure may reliably apply `r5-set-cdr!' to the pairs it is given
without altering the sequence of execution.

  (pair-for-each (lambda (pair) (r5-display pair) (newline)) '(a b c))
  => (a b c)
     (b c)
     (c)

At least one of the list arguments must be finite."
  (s1-check-arg #'r5-procedure? proc #'s1-pair-for-each)
  (if (r5-pair? lists)

      (r5-let lp ((lists (cons lis1 lists)))
	(r5-let ((tails (s1-%cdrs lists)))
	  (if (r5-pair? tails)
	      (r5-begin (apply proc lists)
                        (r5-tail-call lp tails)))))

    ;; Fast path.
    (r5-let lp ((lis lis1))
      (if (not (s1-null-list? lis))
          (r5-let ((tail (cdr lis)))	; Grab the cdr now,
            (funcall proc lis)		; in case PROC SET-CDR!s LIS.
            (r5-tail-call lp tail))))))

;;; We stop when LIS1 runs out, not when any list runs out.
(r5-define (s1-map! f lis1 &rest lists)
  "Linear-update variant of `s1-map' -- `s1-map!' is allowed, but not required,
to alter the cons cells of LIS1 to construct the result list.

The dynamic order in which the various applications of F are made is not
specified. In the N-ary case, LIS2, LIS3, ... must have at least as many
elements as LIS1."
  (s1-check-arg #'r5-procedure? f #'s1-map!)
  (if (r5-pair? lists)
      (r5-let lp ((lis1 lis1) (lists lists))
	(if (not (s1-null-list? lis1))
	    (s8-receive (heads tails) (s1-%cars+cdrs/no-test lists)
	      (r5-set-car! lis1 (apply f (car lis1) heads))
	      (r5-tail-call lp (cdr lis1) tails))))

    ;; Fast path.
    (s1-pair-for-each (lambda (pair) (r5-set-car! pair (funcall f (car pair))))
                      lis1))
  lis1)


;;; Map F across L, and save up all the non-false results.
(r5-define (s1-filter-map f lis1 &rest lists)
  "Like `s1-map', but only true values are saved.

  (s1-filter-map (lambda (x) (and (r5-number? x) (r5* x x))) '(a 1 b 3 c 7))
  => (1 9 49)

The dynamic order in which the various applications of F are made is not
specified.

At least one of the list arguments must be finite."
  (s1-check-arg #'r5-procedure? f #'s1-filter-map)
  (if (r5-pair? lists)
      (r5-let recur ((lists (cons lis1 lists)))
	(s8-receive (cars cdrs) (s1-%cars+cdrs lists)
	  (if (r5-pair? cars)
	      (r5-cond ((apply f cars) => (lambda (x)
                                            (cons x (funcall recur cdrs))))
                       (else (r5-tail-call recur cdrs))) ; Tail call in this arm.
            '())))

    ;; Fast path.
    (r5-let recur ((lis lis1))
      (if (s1-null-list? lis) lis
        (r5-let ((tail (funcall recur (cdr lis))))
          (r5-cond ((funcall f (car lis)) => (lambda (x) (cons x tail)))
                   (else tail)))))))


;;; Map F across lists, guaranteeing to go left-to-right.
;;; NOTE: Some implementations of R5RS MAP are compliant with this spec;
;;; in which case this procedure may simply be defined as a synonym for MAP.

(r5-define (s1-map-in-order f lis1 &rest lists)
  "A variant of the `s1-map' procedure that guarantees to apply F across the
elements of the LISI arguments in a left-to-right order. This is useful for
mapping procedures that both have side effects and return useful values.

At least one of the list arguments must be finite."
  (s1-check-arg #'r5-procedure? f #'s1-map-in-order)
  (if (r5-pair? lists)
      (r5-let recur ((lists (cons lis1 lists)))
	(s8-receive (cars cdrs) (s1-%cars+cdrs lists)
	  (if (r5-pair? cars)
	      (r5-let ((x (apply f cars)))		; Do head first,
		(cons x (funcall recur cdrs)))		; then tail.
            '())))

    ;; Fast path.
    (r5-let recur ((lis lis1)
                   (accum '()))
      (if (s1-null-list? lis) (reverse accum)
        (r5-let ((tail (cdr lis))
                 (x (funcall f (car lis))))		; Do head first,
          (r5-tail-call recur tail (cons x accum)))))))	; then tail.


;;; We extend MAP to handle arguments of unequal length.
(defalias 's1-map #'s1-map-in-order
  "[R5RS+] F is a procedure taking as many arguments as there are list
arguments and returning a single value. `s1-map' applies F element-wise to the
elements of the lists and returns a list of the results, in order. The dynamic
order in which F is applied to the elements of the lists is unspecified.

  (s1-map #'cadr '((a b) (d e) (g h))) => (b e h)
  (s1-map (lambda (n) (r5-expt n n)) '(1 2 3 4 5)) => (1 4 27 256 3125)
  (s1-map #'r5+ '(1 2 3) '(4 5 6)) => (5 7 9)

  (r5-let ((count 0))
    (s1-map (lambda (ignored)
              (r5-set! count (r5+ count 1))
              count)
            '(a b)))
  => (1 2) or (2 1)")


;;; filter, remove, partition
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; FILTER, REMOVE, PARTITION and their destructive counterparts do not
;;; disorder the elements of their argument.

;; This FILTER shares the longest tail of L that has no deleted elements.
;; If Scheme had multi-continuation calls, they could be made more efficient.

(r5-define (s1-filter pred lis)			; Sleazing with EQ? makes this
  "Return all the elements of LIS that satisfy predicate PRED. The list is not
disordered -- elements that appear in the result list occur in the same order
as they occur in the argument list. The returned list may share a common tail
with the argument list. The dynamic order in which the various applications of
PRED are made is not specified.

  (s1-filter #'r5-even? '(0 7 8 8 43 -4)) => (0 8 8 -4)"
  (s1-check-arg #'r5-procedure? pred #'s1-filter)		; one faster.
  (r5-let recur ((lis lis))
    (if (s1-null-list? lis) lis			; Use NOT-PAIR? to handle dotted lists.
      (r5-let ((head (car lis))
               (tail (cdr lis)))
        (if (funcall pred head)
            (r5-let ((new-tail (funcall recur tail)))	; Replicate the RECUR call so
              (if (r5-eq? tail new-tail) lis
                (cons head new-tail)))
          (r5-tail-call recur tail))))))			; this one can be a tail call.


;;; Another version that shares longest tail.
;(define (filter pred lis)
;  (receive (ans no-del?)
;      ;; (recur l) returns L with (pred x) values filtered.
;      ;; It also returns a flag NO-DEL? if the returned value
;      ;; is EQ? to L, i.e. if it didn't have to delete anything.
;      (let recur ((l l))
;	(if (null-list? l) (values l #t)
;	    (let ((x  (car l))
;		  (tl (cdr l)))
;	      (if (pred x)
;		  (receive (ans no-del?) (recur tl)
;		    (if no-del?
;			(values l #t)
;			(values (cons x ans) #f)))
;		  (receive (ans no-del?) (recur tl) ; Delete X.
;		    (values ans #f))))))
;    ans))



;(define (filter! pred lis)			; Things are much simpler
;  (let recur ((lis lis))			; if you are willing to
;    (if (pair? lis)				; push N stack frames & do N
;        (cond ((pred (car lis))		; SET-CDR! writes, where N is
;               (set-cdr! lis (recur (cdr lis))); the length of the answer.
;               lis)
;              (else (recur (cdr lis))))
;        lis)))


;;; This implementation of FILTER!
;;; - doesn't cons, and uses no stack;
;;; - is careful not to do redundant SET-CDR! writes, as writes to memory are
;;;   usually expensive on modern machines, and can be extremely expensive on
;;;   modern Schemes (e.g., ones that have generational GC's).
;;; It just zips down contiguous runs of in and out elts in LIS doing the
;;; minimal number of SET-CDR!s to splice the tail of one run of ins to the
;;; beginning of the next.

(r5-define (s1-filter! pred lis)
  "Linear-update variant of `s1-filter'. This procedure is allowed, but not
required, to alter the cons cells in the argument list to construct the result
lists."
  (s1-check-arg #'r5-procedure? pred #'s1-filter!)
  (r5-let lp ((ans lis))
    (r5-cond ((s1-null-list? ans)       ans)			; Scan looking for
             ((not (funcall pred (car ans))) (r5-tail-call lp (cdr ans)))	; first cons of result.

             ;; ANS is the eventual answer.
             ;; SCAN-IN: (CDR PREV) = LIS and (CAR PREV) satisfies PRED.
             ;;          Scan over a contiguous segment of the list that
             ;;          satisfies PRED.
             ;; SCAN-OUT: (CAR PREV) satisfies PRED. Scan over a contiguous
             ;;           segment of the list that *doesn't* satisfy PRED.
             ;;           When the segment ends, patch in a link from PREV
             ;;           to the start of the next good segment, and jump to
             ;;           SCAN-IN.
             (else
              (r5-letrec ((scan-in
                           (lambda (prev lis)
                             (if (r5-pair? lis)
                                 (if (funcall pred (car lis))
                                     (r5-tail-call scan-in lis (cdr lis))
                                   (r5-tail-call scan-out prev (cdr lis))))))
                          (scan-out
                           (lambda (prev lis)
                             (r5-let lp ((lis lis))
                               (if (r5-pair? lis)
                                   (if (funcall pred (car lis))
                                       (r5-begin
                                        (r5-set-cdr! prev lis)
                                        (funcall scan-in lis (cdr lis)))
                                     (r5-tail-call lp (cdr lis)))
                                 (r5-set-cdr! prev lis))))))
                (funcall scan-in ans (cdr ans))
                ans)))))



;;; Answers share common tail with LIS where possible;
;;; the technique is slightly subtle.

(r5-define (s1-partition pred lis)
  "Partitions the elements of LIS with predicate PRED, and returns two values:
the list of in-elements and the list of out-elements. The list is not
disordered -- elements occur in the result lists in the same order as they
occur in the argument list. The dynamic order in which the various applications
of PRED are made is not specified. One of the returned lists may share a common
tail with the argument list.

  (s1-partition #'r5-symbol? '(one 2 3 four five 6))
  => (one four five)
     (2 3 6)"
  (s1-check-arg #'r5-procedure? pred #'s1-partition)
  (r5-let recur ((lis lis))
    (if (s1-null-list? lis) (r5-values lis lis)	; Use NOT-PAIR? to handle dotted lists.
      (r5-let ((elt (car lis))
               (tail (cdr lis)))
        (s8-receive (in out) (funcall recur tail)
          (if (funcall pred elt)
              (r5-values (if (r5-pair? out) (cons elt in) lis) out)
            (r5-values in (if (r5-pair? in) (cons elt out) lis))))))))



;(define (partition! pred lis)			; Things are much simpler
;  (let recur ((lis lis))			; if you are willing to
;    (if (null-list? lis) (values lis lis)	; push N stack frames & do N
;        (let ((elt (car lis)))			; SET-CDR! writes, where N is
;          (receive (in out) (recur (cdr lis))	; the length of LIS.
;            (cond ((pred elt)
;                   (set-cdr! lis in)
;                   (values lis out))
;                  (else (set-cdr! lis out)
;                        (values in lis))))))))


;;; This implementation of PARTITION!
;;; - doesn't cons, and uses no stack;
;;; - is careful not to do redundant SET-CDR! writes, as writes to memory are
;;;   usually expensive on modern machines, and can be extremely expensive on
;;;   modern Schemes (e.g., ones that have generational GC's).
;;; It just zips down contiguous runs of in and out elts in LIS doing the
;;; minimal number of SET-CDR!s to splice these runs together into the result
;;; lists.

(r5-define (s1-partition! pred lis)
  "Linear-update variant of `s1-partition'. This procedure is allowed, but not
required, to alter the cons cells in the argument list to construct the result
lists."
  (s1-check-arg #'r5-procedure? pred #'s1-partition!)
  (if (s1-null-list? lis) (r5-values lis lis)

    ;; This pair of loops zips down contiguous in & out runs of the
    ;; list, splicing the runs together. The invariants are
    ;;   SCAN-IN:  (cdr in-prev)  = LIS.
    ;;   SCAN-OUT: (cdr out-prev) = LIS.
    (r5-letrec ((scan-in
                 (lambda (in-prev out-prev lis)
                   (r5-let lp ((in-prev in-prev) (lis lis))
                     (if (r5-pair? lis)
                         (if (funcall pred (car lis))
                             (r5-tail-call lp lis (cdr lis))
                           (r5-begin (r5-set-cdr! out-prev lis)
                                     (funcall scan-out in-prev lis (cdr lis))))
                       (r5-set-cdr! out-prev lis))))) ; Done.

                (scan-out
                 (lambda (in-prev out-prev lis)
                   (r5-let lp ((out-prev out-prev) (lis lis))
                     (if (r5-pair? lis)
                         (if (funcall pred (car lis))
                             (r5-begin (r5-set-cdr! in-prev lis)
                                       (funcall scan-in lis out-prev (cdr lis)))
                           (r5-tail-call lp lis (cdr lis)))
                       (r5-set-cdr! in-prev lis)))))) ; Done.

      ;; Crank up the scan&splice loops.
      (if (funcall pred (car lis))
          ;; LIS begins in-list. Search for out-list's first pair.
          (r5-let lp ((prev-l lis) (l (cdr lis)))
            (r5-cond ((not (r5-pair? l)) (r5-values lis l))
                     ((funcall pred (car l)) (r5-tail-call lp l (cdr l)))
                     (else (funcall scan-out prev-l l (cdr l))
                           (r5-values lis l))))	; Done.

        ;; LIS begins out-list. Search for in-list's first pair.
        (r5-let lp ((prev-l lis) (l (cdr lis)))
          (r5-cond ((not (r5-pair? l)) (r5-values l lis))
                   ((funcall pred (car l))
                    (funcall scan-in l prev-l (cdr l))
                    (r5-values l lis))		; Done.
                   (else (r5-tail-call lp l (cdr l)))))))))


;;; Inline us, please.
(r5-define (s1-remove  pred l)
  "Returns L without the elements that satisfy predicate PRED:

  (s1-filter (lambda (x) (not (funcall PRED x))) L))

The list is not disordered -- elements that appear in the result list occur in
the same order as they occur in the argument list. The returned list may share
a common tail with the argument list. The dynamic order in which the various
applications of PRED are made is not specified.

  (s1-remove #'r5-even? '(0 7 8 8 43 -4)) => (7 43)"
  (s1-filter  (lambda (x) (not (funcall pred x))) l))
(r5-define (s1-remove! pred l)
  "Linear-update variant of `s1-remove'. This procedure is allowed, but not
required, to alter the cons cells in the argument list to construct the result
lists."
  (s1-filter! (lambda (x) (not (funcall pred x))) l))



;;; Here's the taxonomy for the DELETE/ASSOC/MEMBER functions.
;;; (I don't actually think these are the world's most important
;;; functions -- the procedural FILTER/REMOVE/FIND/FIND-TAIL variants
;;; are far more general.)
;;;
;;; Function			Action
;;; ---------------------------------------------------------------------------
;;; remove pred lis		Delete by general predicate
;;; delete x lis [=]		Delete by element comparison
;;;
;;; find pred lis		Search by general predicate
;;; find-tail pred lis		Search by general predicate
;;; member x lis [=]		Search by element comparison
;;;
;;; assoc key lis [=]		Search alist by key comparison
;;; alist-delete key alist [=]	Alist-delete by key comparison

(r5-define (s1-delete x lis &optional elt=)
  "`s1-delete' uses the comparison procedure ELT=, which defaults to
`r5-equal?', to find all elements of LIS that are equal to X, and deletes them
from LIS. The dynamic order in which the various applications of ELT= made is
not specified.

The list is not disordered -- elements that appear in the result list occur in
the same order as they occur in the argument list. The result may share a
common tail with the argument list.

Note that fully general element deletion can be performed with the `s1-remove'
and `s1-remove!' procedures, e.g.:

  ;; Delete all the even elements from LIS:
  (s1-remove #'r5-even? LIS)

The comparison procedure is used in this way: (funcall ELT= X E_I). That is, X
is always the first argument, and a list element is always the second argument.
The comparison procedure will be used to compare each element of LIS exactly
once; the order in which it is applied to the various E_I is not specified.
Thus, one can reliably remove all the numbers greater than five from a list
with (s1-delete 5 LIS #'r5<)"
  (r5-let ((elt= (if (r5-null? elt=) #'r5-equal? elt=)))
    (s1-filter (lambda (y) (not (funcall elt= x y))) lis)))

(r5-define (s1-delete! x lis &optional elt=)
  "`s1-delete!' is the linear-update variant of `s1-delete'. It is allowed, but
not required, to alter the cons cells in its argument list to construct the
result."
  (r5-let ((elt= (if (r5-null? elt=) #'r5-equal? elt=)))
    (s1-filter! (lambda (y) (not (funcall elt= x y))) lis)))

;;; Extended from R4RS to take an optional comparison argument.
(r5-define (s1-member x lis &optional elt=)
  "[R5RS+] This procedure returns the first sublist of LIS whose car is X,
where the sublists of LIS are the non-empty lists returned by (s1-drop LIS I)
for I less than the length of LIS. If X does not occur in LIS, then () is
returned. `s1-member' uses `r5-eq?' to compare X with the elements of LIS.

  (s1-member (list 'a) '(b (a) c)) => ((a) c)

`s1-member' is extended from its R5RS definition to allow the client to pass in
an optional equality procedure ELT= to compare keys.

The comparison procedure is used to compare the elements E_I of LIS to the key
X in this way:

  (funcall ELT= E_I) ; LIS is (E1 ... EN)

That is, the first argument is always X, and the second argument is one of the
list elements. Thus one can reliably find the first element of LIS that is
greater than five with (s1-member 5 LIS #'r5<)

Note that fully general list searching may be performed with the `s1-find-tail'
and `s1-find' procedures, e.g.

  (s1-find-tail #'r5-even? LIS) ; Find the first elt with an even key."
  (r5-let ((elt= (if (r5-null? elt=) #'r5-equal? elt=)))
    (s1-find-tail (lambda (y) (funcall elt= x y)) lis)))

;;; R4RS, hence we don't bother to define.
;;; The MEMBER and then FIND-TAIL call should definitely
;;; be inlined for MEMQ & MEMV.
;(define (memq    x lis) (member x lis eq?))
;(define (memv    x lis) (member x lis eqv?))


;;; right-duplicate deletion
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; delete-duplicates delete-duplicates!
;;;
;;; Beware -- these are N^2 algorithms. To efficiently remove duplicates
;;; in long lists, sort the list to bring duplicates together, then use a
;;; linear-time algorithm to kill the dups. Or use an algorithm based on
;;; element-marking. The former gives you O(n lg n), the latter is linear.

(r5-define (s1-delete-duplicates lis &optional elt=)
  "`s1-delete-duplicates' removes duplicate elements from the list argument. If
there are multiple equal elements in the argument list, the result list only
contains the first or leftmost of these elements in the result. The order of
these surviving elements is the same as in the original list --
`s1-delete-duplicates' does not disorder the list (hence it is useful for
\"cleaning up\" association lists).

The ELT= parameter is used to compare the elements of the list; it defaults to
`r5-equal?'. If X comes before Y in LIS, then the comparison is performed
(funcall ELT= X Y). The comparison procedure will be used to compare each pair
of elements in LIS no more than once; the order in which it is applied to the
various pairs is not specified.

Implementations of `s1-delete-duplicates' are allowed to share common tails
between argument and result lists -- for example, if the list argument contains
only unique elements, it may simply return exactly this list.

Be aware that, in general, `s1-delete-duplicates' runs in time O(N^2) for
N-element lists. Uniquifying long lists can be accomplished in O(N lg N) time
by sorting the list to bring equal elements together, then using a linear-time
algorithm to remove equal elements. Alternatively, one can use algorithms based
on element-marking, with linear-time results.

  (s1-delete-duplicates '(a b a c a b c z)) => (a b c z)

  ;; Clean up an alist:
  (s1-delete-duplicates '((a . 3) (b . 7) (a . 9) (c . 1))
                         (lambda (x y) (r5-eq? (car x) (car y))))
  => ((a . 3) (b . 7) (c . 1))"
  (r5-let ((elt= (if (r5-null? elt=) #'r5-equal? elt=)))
    (s1-check-arg #'r5-procedure? elt= #'s1-delete-duplicates)
    (r5-let recur ((lis lis))
      (if (s1-null-list? lis) lis
        (let* ((x (car lis))
               (tail (cdr lis))
               (new-tail (funcall recur (s1-delete x tail elt=))))
          (if (r5-eq? tail new-tail) lis (cons x new-tail)))))))

(r5-define (s1-delete-duplicates! lis &optional elt=)
  "`s1-delete-duplicates!' is the linear-update variant of
`s1-delete-duplicates'; it is allowed, but not required, to alter the cons
cells in its argument list to construct the result."
  (r5-let ((elt= (if (r5-null? elt=) #'r5-equal? elt=)))
    (s1-check-arg #'r5-procedure? elt= #'s1-delete-duplicates!)
    (r5-let recur ((lis lis))
      (if (s1-null-list? lis) lis
        (let* ((x (car lis))
               (tail (cdr lis))
               (new-tail (funcall recur (s1-delete! x tail elt=))))
          (if (r5-eq? tail new-tail) lis (cons x new-tail)))))))


;;; alist stuff
;;;;;;;;;;;;;;;

;;; Extended from R4RS to take an optional comparison argument.
(r5-define (s1-assoc key alist &optional elt=)
  "[R5RS+] ALIST must be an association list -- a list of pairs. This procedure
finds the first pair in ALIST whose car field is KEY, and returns that pair. If
no pair in ALIST has KEY as its car, then () is returned. `s1-assoc' uses
`r5-equal?' to compare KEY with the car fields of the pairs in ALIST.

  (r5-define e '((a 1) (b 2) (c 3)))
  (s1-assoc (list 'a) '(((a)) ((b)) ((c)))) => ((a))

`s1-assoc' is extended from its R5RS definition to allow the client to pass in
an optional equality procedure ELT= used to compare keys.

The comparison procedure is used to compare the elements EI of ALIST to the KEY
parameter in this way:

  (funcall ELT= KEY (car EI)) ; ALIST is (E1 ... EN)

That is, the first argument is always KEY, and the second argument is one of
the list elements. Thus one can reliably find the first entry of ALIST whose
KEY is greater than five with (s1-assoc 5 ALIST #'r5<)

Note that fully general alist searching may be performed with the
`s1-find-tail' and `s1-find' procedures, e.g.

  ;; Look up the first association in ALIST with an even key:
  (s1-find (lambda (a) (r5-even? (car a))) ALIST)"
  (r5-let ((elt= (if (r5-null? elt=) #'r5-equal? elt=)))
    (s1-find (lambda (entry) (funcall elt= key (car entry))) alist)))

(r5-define (s1-alist-cons key datum alist)
  "= (cons (cons KEY DATUM) ALIST)

Cons a new alist entry mapping KEY -> DATUM onto ALIST."
  (cons (cons key datum) alist))

(r5-define (s1-alist-copy alist)
  "Make a fresh copy of ALIST. This means copying each pair that forms an
association as well as the spine of the list, i.e.

  (s1-map (lambda (elt) (cons (car elt) (cdr elt))) ALIST)"
  (s1-map (lambda (elt) (cons (car elt) (cdr elt)))
          alist))

(r5-define (s1-alist-delete key alist &optional elt=)
  "`s1-alist-delete' deletes all associations from ALIST with the given KEY,
using key-comparison procedure ELT=, which defaults to `r5-equal?'. The dynamic
order in which the various applications of ELT= are made is not specified.

Return values may share common tails with the ALIST argument. The alist is not
disordered -- elements that appear in the result alist occur in the same order
as they occur in the argument alist.

The comparison procedure is used to compare the element keys K_I of ALIST's
entries to the KEY parameter in this way: (funcall ELT= KEY K_I). Thus, one can
reliably remove all entries of ALIST whose KEY is greater than five with
(s1-alist-delete 5 ALIST #'r5<)"
  (r5-let ((elt= (if (r5-null? elt=) #'r5-equal? elt=)))
    (s1-filter (lambda (elt) (not (funcall elt= key (car elt)))) alist)))

(r5-define (s1-alist-delete! key alist &optional elt=)
  "`s1-alist-delete!' is the linear-update variant of `s1-alist-delete'. It is
allowed, but not required, to alter cons cells from the ALIST parameter to
construct the result."
  (r5-let ((elt= (if (r5-null? elt=) #'r5-equal? elt=)))
    (s1-filter! (lambda (elt) (not (funcall elt= key (car elt)))) alist)))


;;; find find-tail take-while drop-while span break any every list-index
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(r5-define (s1-find pred list)
  "Return the first element of LIST that satisfies predicate PRED; false if no
element does.

  (s1-find #'r5-even? '(3 1 4 1 5 9)) => 4

Note that `s1-find' has an ambiguity in its lookup semantics -- if `s1-find'
returns (), you cannot tell (in general) if it found a () element that
satisfied PRED, or if it did not find any element at all. In many situations,
this ambiguity cannot arise -- either the list being searched is known not to
contain any () elements, or the list is guaranteed to have an element
satisfying PRED. However, in cases where this ambiguity can arise, you should
use `s1-find-tail' instead of `s1-find' -- `s1-find-tail' has no such
ambiguity:

  (r5-cond ((s1-find-tail PRED LIST) => (lambda (pair) ...)) ; Handle (CAR PAIR)
           (else ...)) ; Search failed."
  (r5-cond ((s1-find-tail pred list) => #'car)
           (else '())))

(r5-define (s1-find-tail pred list)
  "Return the first pair of LIST whose car satisfies PRED. If no pair does,
return false.

`s1-find-tail' can be viewed as a general-predicate variant of the `s1-member'
function.

Examples:

  (s1-find-tail #'r5-even? '(3 1 37 -8 -5 0 0)) => (-8 -5 0 0)
  (s1-find-tail #'r5-even? '(3 1 37 -5)) => ()

  ;; MEMBER X LIST:
  (s1-find-tail (lambda (elt) (r5-equal? X elt)) LIST)

In the circular-list case, this procedure \"rotates\" the list.

`s1-find-tail' is essentially `s1-drop-while', where the sense of the predicate
is inverted: `s1-find-tail' searches until it finds an element satisfying the
predicate; `s1-drop-while' searches until it finds an element that doesn't
satisfy the predicate."
  (s1-check-arg #'r5-procedure? pred #'s1-find-tail)
  (r5-let lp ((list list))
    (and (not (s1-null-list? list))
	 (if (funcall pred (car list)) list
           (r5-tail-call lp (cdr list))))))

(r5-define (s1-take-while pred lis)
  "Returns the longest initial prefix of LIS whose elements all satisfy the
predicate PRED.

  (s1-take-while #'r5-even? '(2 18 3 10 22 9)) => (2 18)"
  (s1-check-arg #'r5-procedure? pred #'s1-take-while)
  (r5-let recur ((lis lis))
    (if (s1-null-list? lis) '()
      (r5-let ((x (car lis)))
        (if (funcall pred x)
            (cons x (funcall recur (cdr lis)))
          '())))))

(r5-define (s1-drop-while pred lis)
  "Drops the longest initial prefix of LIS whose elements all satisfy the
predicate PRED, and returns the rest of the list.

  (s1-drop-while #'r5-even? '(2 18 3 10 22 9)) => (3 10 22 9)

The circular-list case may be viewed as \"rotating\" the list."
  (s1-check-arg #'r5-procedure? pred #'s1-drop-while)
  (r5-let lp ((lis lis))
    (if (s1-null-list? lis) '()
      (if (funcall pred (car lis))
          (r5-tail-call lp (cdr lis))
        lis))))

(r5-define (s1-take-while! pred lis)
  "Returns the longest initial prefix of LIS whose elements all satisfy the
predicate PRED.

`s1-take-while!' is the linear-update variant of `s1-take-while'. It is
allowed, but not required, to alter the argument list to produce the result."
  (s1-check-arg #'r5-procedure? pred #'s1-take-while!)
  (if (or (s1-null-list? lis) (not (funcall pred (car lis)))) '()
    (r5-begin (r5-let lp ((prev lis) (rest (cdr lis)))
                (if (r5-pair? rest)
                    (r5-let ((x (car rest)))
                      (if (funcall pred x) (r5-tail-call lp rest (cdr rest))
                        (r5-set-cdr! prev '())))))
              lis)))

(r5-define (s1-span pred lis)
  "`s1-span' splits the list into the longest initial prefix whose elements all
satisfy PRED, and the remaining tail.

In other words: `s1-span' finds the initial span of elements satisfying PRED.

`s1-span' is equivalent to

  (s1-values (s1-take-while PRED LIS)
             (s1-drop-while PRED LIS))

  (s1-span #'r5-even? '(2 18 3 10 22 9))
  => (2 18)
     (3 10 22 9)"
  (s1-check-arg #'r5-procedure? pred #'s1-span)
  (r5-let recur ((lis lis))
    (if (s1-null-list? lis) (r5-values '() '())
      (r5-let ((x (car lis)))
        (if (funcall pred x)
            (s8-receive (prefix suffix) (funcall recur (cdr lis))
              (r5-values (cons x prefix) suffix))
          (r5-values '() lis))))))

(r5-define (s1-span! pred lis)
  "`s1-span!' is the linear-update variant of `s1-span'. It is allowed, but not
required, to alter the argument list to produce the result."
  (s1-check-arg #'r5-procedure? pred #'s1-span!)
  (if (or (s1-null-list? lis) (not (funcall pred (car lis))))
      (r5-values '() lis)
    (r5-let ((suffix (r5-let lp ((prev lis) (rest (cdr lis)))
                       (if (s1-null-list? rest) rest
                         (r5-let ((x (car rest)))
                           (if (funcall pred x)
                               (r5-tail-call lp rest (cdr rest))
                             (r5-begin (r5-set-cdr! prev '())
				       rest)))))))
      (r5-values lis suffix))))


(r5-define (s1-break  pred lis)
  "`s1-break' splits the list into the longest initial prefix whose elements
all does not satisfy PRED, and the remaining tail.

In other words: `s1-break' breaks the list at the first element satisfying
PRED.

  (s1-break #'r5-even? '(3 1 4 1 5 9)
   => (3 1)
      (4 1 5 9)"
  (s1-span  (lambda (x) (not (funcall pred x))) lis))
(r5-define (s1-break! pred lis)
  "`s1-break!' is the linear-update variant of `s1-break'. It is allowed, but
not required, to alter the argument list to produce the result."
  (s1-span! (lambda (x) (not (funcall pred x))) lis))

(r5-define (s1-any pred lis1 &rest lists)
  "Applies the predicate across the lists, returning true if the predicate
returns true on any application.

If there are N list arguments LIS1 ... LISN, then PRED must be a procedure
taking N arguments and returning a boolean result.

`s1-any' applies PRED to the first elements of the LISI parameters. If this
application returns a true value, `s1-any' immediately returns that value.
Otherwise, it iterates, applying PRED to the second elements of the LISI
parameters, then the third, and so forth. The iteration stops when a true value
is produced or one of the lists runs out of values; in the latter case,
`s1-any' returns (). The application of PRED to the last element of the lists
is a tail call."
  (s1-check-arg #'r5-procedure? pred #'s1-any)
  (if (r5-pair? lists)

      ;; N-ary case
      (s8-receive (heads tails) (s1-%cars+cdrs (cons lis1 lists))
	(and (r5-pair? heads)
	     (r5-let lp ((heads heads) (tails tails))
	       (s8-receive (next-heads next-tails) (s1-%cars+cdrs tails)
		 (if (r5-pair? next-heads)
		     (or (apply pred heads)
                         (r5-tail-call lp next-heads next-tails))
                   (apply pred heads)))))) ; Last PRED app is tail call.

    ;; Fast path
    (and (not (s1-null-list? lis1))
         (r5-let lp ((head (car lis1)) (tail (cdr lis1)))
           (if (s1-null-list? tail)
               (funcall pred head)		; Last PRED app is tail call.
             (or (funcall pred head)
                 (r5-tail-call lp (car tail) (cdr tail))))))))


;(define (every pred list)              ; Simple definition.
;  (let lp ((list list))                ; Doesn't return the last PRED value.
;    (or (not (pair? list))
;        (and (pred (car list))
;             (lp (cdr list))))))

(r5-define (s1-every pred lis1 &rest lists)
  "Applies the predicate across the lists, returning true if the predicate
returns true on every application.

If there are N list arguments LIS1 ... LISN, then PRED must be a procedure
taking N arguments and returning a boolean result.

`s1-every' applies PRED to the first elements of the LISI parameters. If this
application returns false, `s1-every' immediately returns false. Otherwise, it
iterates, applying PRED to the second elements of the LISI parameters, then the
third, and so forth. The iteration stops when a false value is produced or one
of the lists runs out of values. In the latter case, `s1-every' returns the
true value produced by its final application of PRED. The application of PRED
to the last element of the lists is a tail call.

If one of the LISI has no elements, `s1-every' simply returns t.

Like `s1-any', `s1-every''s name does not end with a question mark -- this is
to indicate that it does not return a simple boolean (t or ()), but a general
value."
  (s1-check-arg #'r5-procedure? pred #'s1-every)
  (if (r5-pair? lists)

      ;; N-ary case
      (s8-receive (heads tails) (s1-%cars+cdrs (cons lis1 lists))
	(or (not (r5-pair? heads))
	    (r5-let lp ((heads heads) (tails tails))
	      (s8-receive (next-heads next-tails) (s1-%cars+cdrs tails)
		(if (r5-pair? next-heads)
		    (and (apply pred heads)
                         (r5-tail-call lp next-heads next-tails))
                  (apply pred heads)))))) ; Last PRED app is tail call.

    ;; Fast path
    (or (s1-null-list? lis1)
        (r5-let lp ((head (car lis1))  (tail (cdr lis1)))
          (if (s1-null-list? tail)
              (funcall pred head)	; Last PRED app is tail call.
            (and (funcall pred head)
                 (r5-tail-call lp (car tail) (cdr tail))))))))

(r5-define (s1-list-index pred lis1 &rest lists)
  "Return the index of the leftmost element that satisfies PRED.

If there are N list arguments LIS1 ... LISN, then PRED must be a function
taking N arguments and returning a boolean result.

`s1-list-index' applies PRED to the first elements of the LISI parameters. If
this application returns true, `s1-list-index' immediately returns zero.
Otherwise, it iterates, applying PRED to the second elements of the LISI
parameters, then the third, and so forth. When it finds a tuple of list
elements that cause PRED to return true, it stops and returns the zero-based
index of that position in the lists.

The iteration stops when one of the lists runs out of values; in this case,
list-index returns ().

  (s1-list-index #'r5-even? '(3 1 4 1 5 9)) => 2
  (s1-list-index #'r5< '(3 1 4 1 5 9 2 5 6) '(2 7 1 8 2)) => 1
  (s1-list-index #'r5= '(3 1 4 1 5 9 2 5 6) '(2 7 1 8 2)) => ()"
  (s1-check-arg #'r5-procedure? pred #'s1-list-index)
  (if (r5-pair? lists)

      ;; N-ary case
      (r5-let lp ((lists (cons lis1 lists)) (n 0))
	(s8-receive (heads tails) (s1-%cars+cdrs lists)
	  (and (r5-pair? heads)
	       (if (apply pred heads) n
                 (r5-tail-call lp tails (r5+ n 1))))))

    ;; Fast path
    (r5-let lp ((lis lis1) (n 0))
      (and (not (s1-null-list? lis))
           (if (funcall pred (car lis))
               n
             (r5-tail-call lp (cdr lis) (r5+ n 1)))))))

;;; Reverse
;;;;;;;;;;;

;R4RS, so not defined here.
;(define (reverse lis) (fold cons '() lis))

;(define (reverse! lis)
;  (pair-fold (lambda (pair tail) (set-cdr! pair tail) pair) '() lis))

(r5-define (s1-reverse! lis)
  "`s1-reverse!' is the linear-update variant of `reverse'. It is permitted,
but not required, to alter the argument's cons cells to produce the reversed
list."
  (r5-let lp ((lis lis) (ans '()))
    (if (s1-null-list? lis) ans
      (r5-let ((tail (cdr lis)))
        (r5-set-cdr! lis ans)
        (r5-tail-call lp tail lis)))))

;;; Lists-as-sets
;;;;;;;;;;;;;;;;;

;;; This is carefully tuned code; do not modify casually.
;;; - It is careful to share storage when possible;
;;; - Side-effecting code tries not to perform redundant writes.
;;; - It tries to avoid linear-time scans in special cases where constant-time
;;;   computations can be performed.
;;; - It relies on similar properties from the other list-lib procs it calls.
;;;   For example, it uses the fact that the implementations of MEMBER and
;;;   FILTER in this source code share longest common tails between args
;;;   and results to get structure sharing in the lset procedures.

(r5-define (s1-%lset2<= elt= lis1 lis2)
  (s1-every (lambda (x) (s1-member x lis2 elt=)) lis1))

(r5-define (s1-lset<= elt= &rest lists)
  "Returns true iff every LIST_I is a subset of LIST_I+1, using ELT= for the
element-equality procedure. List A is a subset of list B if every element in A
is equal to some element of B. When performing an element comparison, the ELT=
procedure's first argument is an element of A; its second, an element of B.

  (s1-lset<= #'r5-eq? '(a) '(a b a) '(a b c c)) => t
  (s1-lset<= #'r5-eq?) => t             ; Trivial cases
  (s1-lset<= #'r5-eq? '(a)) => t"
  (s1-check-arg #'r5-procedure? elt= #'s1-lset<=)
  (or (not (r5-pair? lists)) ; 0-ary case
      (r5-let lp ((s1 (car lists)) (rest (cdr lists)))
	(or (not (r5-pair? rest))
	    (r5-let ((s2 (car rest))  (rest (cdr rest)))
	      (and (or (r5-eq? s2 s1)	; Fast path
		       (s1-%lset2<= elt= s1 s2)) ; Real test
		   (r5-tail-call lp s2 rest)))))))

(r5-define (s1-lset= elt= &rest lists)
  "Returns true iff every LIST_I is set-equal to LIST_I+1, using ELT= for the
element-equality procedure. \"Set-equal\" simply means that LIST_I is a subset
of LIST_I+1, and LIST_I+1 is a subset of LIST_I. The ELT= procedure's first
argument is an element of LIST_I; its second is an element of LIST_I+1.

  (s1-lset= #'r5-eq? '(b e a) '(a e b) '(e e b a)) => t
  (s1-lset= #'r5-eq?) => t               ; Trivial cases
  (s1-lset= #'r5-eq? '(a)) => t"
  (s1-check-arg #'r5-procedure? elt= #'s1-lset=)
  (or (not (r5-pair? lists)) ; 0-ary case
      (r5-let lp ((s1 (car lists)) (rest (cdr lists)))
	(or (not (r5-pair? rest))
	    (r5-let ((s2   (car rest))
                     (rest (cdr rest)))
	      (and (or (r5-eq? s1 s2)	; Fast path
		       (and (s1-%lset2<= elt= s1 s2) (s1-%lset2<= elt= s2 s1))) ; Real test
		   (r5-tail-call lp s2 rest)))))))


(r5-define (s1-lset-adjoin elt= lis &rest elts)
  "Adds the ELT_I elements not already in the list parameter to the result
list. The result shares a common tail with the list parameter. The new elements
are added to the front of the list, but no guarantees are made about their
order. The ELT= parameter is an equality procedure used to determine if an
ELT_I is already a member of LIS. Its first argument is an element of LIS; its
second is one of the ELT_I.

The list parameter is always a suffix of the result -- even if the list
parameter contains repeated elements, these are not reduced.

  (s1-lset-adjoin #'r5-eq? '(a b c d c e) 'a 'e 'i 'o 'u)
  => (u o i a b c d c e)"
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-adjoin)
  (s1-fold (lambda (elt ans) (if (s1-member elt ans elt=) ans (cons elt ans)))
           lis elts))


(r5-define (s1-lset-union elt= &rest lists)
  "Returns the union of the LISTS, using ELT= for the element-equality
procedure.

The union of lists A and B is constructed as follows:

  If A is the empty list, the answer is B (or a copy of B).

  Otherwise, the result is initialised to be list A (or a copy of A).

  Proceed through the elements of list B in a left-to-right order. If BETA is
  such an element of B, compare every element R of the current result list to
  BETA: (funcall ELT= R BETA). If all comparisons fail, BETA is consed onto the
  front of the result.

However, there is no guarantee that ELT= will be applied to every pair of
arguments from A and B. In particular, if A is `r5-eq?' to B, the operation may
immediately terminate.

In the N-ary case, the two-argument list-union operation is simply folded
across the argument lists.

  (s1-lset-union #'r5-eq? '(a b c d e) '(a e i o u)) => (u o i a b c d e)

  ;; Repeated elements in LIST1 are preserved.
  (s1-lset-union #'r5-eq? '(a a c) '(x a x)) => (x a a c)

  ;; Trivial cases
  (s1-lset-union #'r5-eq?) => ()
  (s1-lset-union #'r5-eq? '(a b c)) => (a b c)"
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-union)
  (s1-reduce (lambda (lis ans)		; Compute ANS + LIS.
               (r5-cond ((r5-null? lis) ans)	; Don't copy any lists
                        ((r5-null? ans) lis) 	; if we don't have to.
                        ((r5-eq? lis ans) ans)
                        (else
                         (s1-fold (lambda (elt ans)
                                    (if (s1-any (lambda (x)
                                                  (funcall elt= x elt))
                                                ans)
                                        ans
                                      (cons elt ans)))
                                  ans lis))))
             '() lists))

(r5-define (s1-lset-union! elt= &rest lists)
  "This is the linear-update variant of `s1-lset-union'. It is allowed, but not
required, to use the cons cells in their first list parameter to construct
their answer. `s1-lset-union!' is permitted to recycle cons cells from any of
its list arguments."
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-union!)
  (s1-reduce (lambda (lis ans)		; Splice new elts of LIS onto the front of ANS.
               (r5-cond ((r5-null? lis) ans)	; Don't copy any lists
                        ((r5-null? ans) lis) 	; if we don't have to.
                        ((r5-eq? lis ans) ans)
                        (else
                         (s1-pair-fold (lambda (pair ans)
                                         (r5-let ((elt (car pair)))
                                           (if (s1-any (lambda (x)
                                                         (funcall elt= x elt))
                                                       ans)
                                               ans
                                             (r5-begin (r5-set-cdr! pair ans)
                                                       pair))))
                                       ans lis))))
             '() lists))


(r5-define (s1-lset-intersection elt= lis1 &rest lists)
  "Returns the intersection of the lists, using ELT= for the element-equality
procedure.

The intersection of lists A and B is comprised of every element of A that is
ELT= to some element of B: (funcall ELT= ALPHA BETA), for ALPHA in A, and BETA
in B. Note this implies that an element which appears in B and multiple times
in list A will also appear multiple times in the result.

The order in which elements appear in the result is the same as they appear in
LIS1 -- that is, `s1-lset-intersection' essentially filters LIS1, without
disarranging element order. The result may share a common tail with LIS1.

In the N-ary case, the two-argument s1-list-intersection operation is simply
folded across the argument lists. However, the dynamic order in which the
applications of ELT= are made is not specified. The procedure may check an
element of LIS1 for membership in every other list before proceeding to
consider the next element of LIS1, or it may completely intersect LIS1 and LIS2
before proceeding to LIS3, or it may go about its work in some third order.

  (s1-lset-intersection #'r5-eq? '(a b c d e) '(a e i o u)) => (a e)

  ;; Repeated elements in LIS1 are preserved.
  (s1-lset-intersection #'r5-eq? '(a x y a) '(x a x z)) => '(a x a)

  (s1-lset-intersection #'r5-eq? '(a b c)) => (a b c)     ; Trivial case"
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-intersection)
  (r5-let ((lists (s1-delete lis1 lists #'r5-eq?))) ; Throw out any LIS1 vals.
    (r5-cond ((s1-any #'s1-null-list? lists) '())		; Short cut
             ((r5-null? lists)          lis1)		; Short cut
             (else (s1-filter (lambda (x)
                                (s1-every (lambda (lis) (s1-member x lis elt=))
                                          lists))
                              lis1)))))

(r5-define (s1-lset-intersection! elt= lis1 &rest lists)
  "This is the linear-update variant of `s1-lset-intersection'. It is allowed,
but not required, to use the cons cells in their first list parameter to
construct their answer."
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-intersection!)
  (r5-let ((lists (s1-delete lis1 lists #'r5-eq?))) ; Throw out any LIS1 vals.
    (r5-cond ((s1-any #'s1-null-list? lists) '())		; Short cut
             ((r5-null? lists)          lis1)		; Short cut
             (else (s1-filter! (lambda (x)
                                 (s1-every (lambda (lis)
                                             (s1-member x lis elt=))
                                           lists))
                               lis1)))))


(r5-define (s1-lset-difference elt= lis1 &rest lists)
  "Returns the difference of the lists, using ELT= for the element-equality
procedure -- all the elements of LIS1 that are not ELT= to any element from one
of the other LISI parameters.

The ELT= procedure's first argument is always an element of LIS1; its second is
an element of one of the other LISI. Elements that are repeated multiple times
in the LIS1 parameter will occur multiple times in the result. The order in
which elements appear in the result is the same as they appear in LIS1 -- that
is, `s1-lset-difference' essentially filters LIS1, without disarranging element
order. The result may share a common tail with LIS1. The dynamic order in which
the applications of ELT= are made is not specified. The procedure may check an
element of LIS1 for membership in every other list before proceeding to
consider the next element of LIS1, or it may completely compute the difference
of LIS1 and LIS2 before proceeding to LIS3, or it may go about its work in some
third order.

  (s1-lset-difference #'r5-eq? '(a b c d e) '(a e i o u)) => (b c d)

  (s1-lset-difference #'r5-eq? '(a b c)) => (a b c) ; Trivial case"
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-difference)
  (r5-let ((lists (s1-filter #'r5-pair? lists)))	; Throw out empty lists.
    (r5-cond ((r5-null? lists)     lis1)	; Short cut
             ((memq lis1 lists) '())	; Short cut
             (else (s1-filter (lambda (x)
                                (s1-every (lambda (lis)
                                            (not (s1-member x lis elt=)))
                                          lists))
                              lis1)))))

(r5-define (s1-lset-difference! elt= lis1 &rest lists)
  "This is the linear-update variant of `s1-lset-difference'. It is allowed,
but not required, to use the cons cells in their first list parameter to
construct their answer."
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-difference!)
  (r5-let ((lists (s1-filter #'r5-pair? lists)))	; Throw out empty lists.
    (r5-cond ((r5-null? lists)     lis1)	; Short cut
             ((memq lis1 lists) '())	; Short cut
             (else (s1-filter! (lambda (x)
                                 (s1-every (lambda (lis)
                                             (not (s1-member x lis elt=)))
                                           lists))
                               lis1)))))


(r5-define (s1-lset-xor elt= &rest lists)
  "Returns the exclusive-or of the sets, using ELT= for the element-equality
procedure. If there are exactly two lists, this is all the elements that appear
in exactly one of the two lists. The operation is associative, and thus extends
to the N-ary case -- the elements that appear in an odd number of the lists.
The result may share a common tail with any of the LISTI parameters.

More precisely, for two lists A and B, A xor B is a list of

  every element ALPHA of A such that there is no element BETA of B such that
  (funcall ELT= ALPHA BETA),

and

  every element BETA of B such that there is no element ALPHA of A such that
  (funcall ELT= BETA ALPHA).

However, an implementation is allowed to assume that ELT= is symmetric -- that
is, that

  (funcall ELT= ALPHA BETA) => (funcall ELT= BETA ALPHA).

This means, for example, that if a comparison (funcall ELT= ALPHA BETA)
produces true for some ALPHA in A and BETA in B, both ALPHA and BETA may be
removed from inclusion in the result.

In the N-ary case, the binary-xor operation is simply folded across the lists.

  (s1-lset-xor #'r5-eq? '(a b c d e) '(a e i o u)) => (u o i b c d)

  ;; Trivial cases.
  (s1-lset-xor #'r5-eq?) => ()
  (s1-lset-xor #'r5-eq? '(a b c d e)) => (a b c d e)"
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-xor)
  (s1-reduce (lambda (b a)			; Compute A xor B:
               ;; Note that this code relies on the constant-time
               ;; short-cuts provided by LSET-DIFF+INTERSECTION,
               ;; LSET-DIFFERENCE & APPEND to provide constant-time short
               ;; cuts for the cases A = (), B = (), and A eq? B. It takes
               ;; a careful case analysis to see it, but it's carefully
               ;; built in.

               ;; Compute a-b and a^b, then compute b-(a^b) and
               ;; cons it onto the front of a-b.
               (s8-receive (a-b a-int-b)   (s1-lset-diff+intersection elt= a b)
                 (r5-cond ((r5-null? a-b)     (s1-lset-difference elt= b a))
                          ((r5-null? a-int-b) (append b a))
                          (else (s1-fold (lambda (xb ans)
                                           (if (s1-member xb a-int-b elt=)
                                               ans
                                             (cons xb ans)))
                                         a-b
                                         b)))))
             '() lists))


(r5-define (s1-lset-xor! elt= &rest lists)
  "This is the linear-update variant of `s1-lset-xor'. It is allowed, but not
required, to use the cons cells in their first list parameter to construct
their answer."
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-xor!)
  (s1-reduce (lambda (b a)			; Compute A xor B:
               ;; Note that this code relies on the constant-time
               ;; short-cuts provided by LSET-DIFF+INTERSECTION,
               ;; LSET-DIFFERENCE & APPEND to provide constant-time short
               ;; cuts for the cases A = (), B = (), and A eq? B. It takes
               ;; a careful case analysis to see it, but it's carefully
               ;; built in.

               ;; Compute a-b and a^b, then compute b-(a^b) and
               ;; cons it onto the front of a-b.
               (s8-receive (a-b a-int-b)   (s1-lset-diff+intersection! elt= a b)
                 (r5-cond ((r5-null? a-b)     (s1-lset-difference! elt= b a))
                          ((r5-null? a-int-b) (s1-append! b a))
                          (else (s1-pair-fold
                                 (lambda (b-pair ans)
                                   (if (s1-member (car b-pair) a-int-b elt=)
                                       ans
                                     (r5-begin (r5-set-cdr! b-pair ans)
                                               b-pair)))
                                 a-b
                                 b)))))
             '() lists))


(r5-define (s1-lset-diff+intersection elt= lis1 &rest lists)
  "Returns two values -- the difference and the intersection of the lists. Is
equivalent to

  (r5-values (s1-lset-difference ELT= LIS1 LIS2 ...)
             (s1-lset-intersection ELT= LIS1
                                   (s1-lset-union ELT= LIS2 ...)))

but can be implemented more efficiently.

The ELT= procedure's first argument is an element of LIS1; its second is an
element of one of the other LISI.

Either of the answer lists may share a common tail with LIS1. This operation
essentially partitions LIS1."
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-diff+intersection)
  (r5-cond ((s1-every #'s1-null-list? lists) (r5-values lis1 '()))	; Short cut
           ((memq lis1 lists)        (r5-values '() lis1))	; Short cut
           (else (s1-partition (lambda (elt)
                                 (not (s1-any (lambda (lis)
                                                (s1-member elt lis elt=))
                                              lists)))
                               lis1))))

(r5-define (s1-lset-diff+intersection! elt= lis1 &rest lists)
  "This is the linear-update variant of `s1-lset-diff+intersection'. It is
allowed, but not required, to use the cons cells in their first list parameter
to construct their answer."
  (s1-check-arg #'r5-procedure? elt= #'s1-lset-diff+intersection!)
  (r5-cond ((s1-every #'s1-null-list? lists) (r5-values lis1 '()))	; Short cut
           ((memq lis1 lists)        (r5-values '() lis1))	; Short cut
           (else (s1-partition! (lambda (elt)
                                  (not (s1-any (lambda (lis)
                                                 (s1-member elt lis elt=))
                                               lists)))
                                lis1))))


(provide 'srfi-1)
